//
//  CollectionViewCell.m
//  Telepoh
//
//  Created by apple on 19/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell



- (void)awakeFromNib
{
    //Changes done directly here, we have an object
}


-(id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        //Changes here after init'ing self
        self.imgUser.layer.cornerRadius =self.imgUser.frame.size.height/2;
        self.imgUser.clipsToBounds = YES;
        
        self.btnIgnore.layer.cornerRadius = 10.0;
        self.btnIgnore.layer.borderColor = [UIColor blackColor].CGColor;
        self.btnIgnore.layer.borderWidth = 2.0;
        self.btnIgnore.clipsToBounds = YES;
        
        self.btnJoin.layer.cornerRadius = 10.0;
        self.btnJoin.layer.borderWidth = 2.0;
        self.btnJoin.clipsToBounds = YES;
        
        
    }
    
    return self;
}
@end
