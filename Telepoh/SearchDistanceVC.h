//
//  SearchDistanceVC.h
//  Telepoh
//
//  Created by apple on 20/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDistanceVC : UIViewController

{
    IBOutlet UIView *vwBackGround;
    
    IBOutlet UILabel *lblMinDistance;
    IBOutlet UISlider *sliderView;
    IBOutlet UILabel *lblMile;
}
- (IBAction)sliderAction:(id)sender;

- (IBAction)btnTappedCancel:(id)sender;
@end
