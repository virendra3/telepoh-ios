//
//  RegistrationViewController.m
//  Telepoh
//
//  Created by apple on 04/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "RegistrationViewController.h"
#import "DateConversions.h"

@interface RegistrationViewController ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    Validation * validateObj;
    NSString * strGender;

    UIDatePicker *datePicker;
    DateConversions *dateConversion;
   
}



@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    validateObj = [Validation validationManager];
    dateConversion = [DateConversions sharedInstance];
    strGender = @"Male";
    
    btnRegistration.layer.cornerRadius = 10.0f;
    btnRegistration.clipsToBounds = YES;
    
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMaximumDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(setPickerDate:) forControlEvents:UIControlEventValueChanged];
    [txtFieldDOB setInputView:datePicker];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(cancelKeyboard:)];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar.items = @[cancelButton];
    txtFieldDOB.inputAccessoryView = toolbar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnRegisterClicked:(id)sender {
    NSString *message = nil;
    
    if (![validateObj validateBlankField:[txtFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Email";
    }
    else if (![validateObj validateEmail:[txtFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Proper Email";
    }
    
    else if (![validateObj validateBlankField:[txtFieldUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter User Name";
    }
    else if (![validateObj validateBlankField:[txtMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter mobile number";
    }
    
    else if (![validateObj validateNumericNumber:[txtMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Proper mobile number";
    }
    else if (![validateObj validateBlankField:[txttFieldCity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter City";
    }
    else if (![validateObj validateBlankField:[txtFieldCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Contry";
    }
    else if (![validateObj validateBlankField:[txtFieldDOB.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter DOB";
    }
    else if (![validateObj validateBlankField:[txtFieldWork.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Work";
    }
    else if (![validateObj validateBlankField:[txtFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Password";
    }
    else if (![validateObj validateBlankField:[txtFieldRetypePassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Reset Password";
    }else if (txtFieldRetypePassword.text != txtFieldPassword.text)
    {
        message =  @"Please enter correct Reset Password";
    }
    
    if ([message length]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else{
        [self RegistrationWebservice];
        
    }
}


#pragma mark - WebService Login method
-(void)RegistrationWebservice
{
    
    NSString *strURL=[NSString stringWithFormat:registrationURL];
    
    strURL = [strURL stringByAppendingString:[NSString stringWithFormat:@"data=%@&UserName=%@&MobileNo=%@&Password=%@&Longitude=1.33&Lattitude=4.56&DeviceID=gnre34535&Gender=%@&City=%@&Country=%@&DOB=%@&work=%@",txtFieldEmail.text,txtFieldUserName.text,txtMobileNumber.text,txtFieldPassword.text,strGender,txttFieldCity.text ,txtFieldCountry.text,txtFieldDOB.text,txtFieldWork.text]];
    
    strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:strURL HttpMethodType:Get_type HttpBodyType:nil Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
             [[NSUserDefaults standardUserDefaults] setValue:[receivedData valueForKey:@"data"] forKey:@"userInfo"];
             [self performSegueWithIdentifier:@"RegToHome" sender:self];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnTappedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnTappedRadio:(id)sender {
    UIButton * btn  = sender;
    
    if (btn.tag == 1) {
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        strGender = @"Male";
    }else{
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        [btnMale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        strGender = @"Female";
    }
    
}

-(void)cancelKeyboard:(UIBarButtonItem *)sender{
    [txtFieldDOB resignFirstResponder];
    txtFieldDOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
    NSLog(@"%@",txtFieldDOB.text);
}

-(void)setPickerDate:(UIDatePicker *)picker{
    txtFieldDOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
    NSLog(@"%@",txtFieldDOB.text);
    
}

#pragma mark - UITextField delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == txtFieldDOB && [txtFieldDOB.text isEqualToString:@""]) {
        NSLog(@"%@",[datePicker date]);
        txtFieldDOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
    }
    return TRUE;
}

@end
