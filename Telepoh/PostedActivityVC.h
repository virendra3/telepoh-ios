//
//  PostedActivityVC.h
//  Telepoh
//
//  Created by apple on 23/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostedActivityVC : UIViewController
{
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *lblNoRecord;
    
}
- (IBAction)btnTappedMenu:(id)sender;

@end
