//
//  PostedActivityVC.m
//  Telepoh
//
//  Created by apple on 23/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "PostedActivityVC.h"
#import "InboxCustomCell.h"
#import "InboxVC.h"

@interface PostedActivityVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    
    NSMutableArray * arrPostedEventList;
}

@end

@implementation PostedActivityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    
    arrPostedEventList = [[NSMutableArray alloc] init];
    [self PostedActivity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    tblView.allowsMultipleSelectionDuringEditing = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrPostedEventList.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InboxCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
    cell.userImage.clipsToBounds = YES;
    
    cell.lblUserName.text =[arrPostedEventList[indexPath.row] valueForKey:@"Title"];
    if([[arrPostedEventList[indexPath.row] valueForKey:@"MaxAllowedPeople"] integerValue]==0)
    {
        cell.lblUserDescription.text =[NSString stringWithFormat:@"Max Allowed People :%@ \nJoined People :%@",@"No Limit",[arrPostedEventList[indexPath.row] valueForKey:@"MaxAllowedPeople"]];
        
    }
    else
    {
        cell.lblUserDescription.text =[NSString stringWithFormat:@"Max Allowed People :%@ \nJoined People :%@",[arrPostedEventList[indexPath.row] valueForKey:@"MaxAllowedPeople"],[arrPostedEventList[indexPath.row] valueForKey:@"JoinedPeople"]];
        
    }
    
    cell.vwStatus.layer.cornerRadius  =  cell.vwStatus.frame.size.width/2;
    cell.vwStatus.clipsToBounds = YES;
    if ([[arrPostedEventList[indexPath.row] valueForKey:@"Status"] isEqualToString:@"Active"]) {
        cell.vwStatus.backgroundColor = [UIColor greenColor];
    }else{
        cell.vwStatus.backgroundColor = [UIColor redColor];
    }

    
//    "EventID": 55,
//    "UserDetailsID": 0,
//    "Title": "ios Test",
//    "UserName": null,
//    "UserProfilePic": null,
//    "UserRatings": 0,
//    "TimeLimit": "2016-04-23T13:47:22.867",
//    "CurrentDistence": 0,
//    "MaxAllowedPeople": 5,
//    "Status": "Active"
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UIStoryboard *storyboard;
//    
//    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    InboxVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"InboxVC"];
//  //  vc.strLastController = @"PostedActivity";
//    vc.strEventID = [arrPostedEventList[indexPath.row] valueForKey:@"EventID"];
//    [self.navigationController pushViewController:vc animated:YES];
    
}

// During startup (-viewDidLoad or in storyboard) do:



// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        [self deletedActivity:[arrPostedEventList[indexPath.row] valueForKey:@"EventID" ] ];
        [arrPostedEventList  removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    
        [tableView endUpdates];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(void)PostedActivity{
    
    NSString *url=[NSString stringWithFormat:MyPostedActivities];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrPostedEventList = [[receivedData valueForKey:@"data"] mutableCopy];
            [tblView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP6" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if (arrPostedEventList.count == 0) {
            lblNoRecord.hidden = false;
            
        }else{
            lblNoRecord.hidden =true;
        }
        
    }];
}

-(void)deletedActivity:(NSString *)strID{
    
    NSString *url=[NSString stringWithFormat:DeletedActivityURL];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[strID intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
//            arrPostedEventList = [[receivedData valueForKey:@"data"] mutableCopy];
//            [tblView reloadData];
            if (arrPostedEventList.count == 0) {
                lblNoRecord.hidden = false;
                
            }else{
                lblNoRecord.hidden =true;
            }
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP7" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}


- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}
@end
