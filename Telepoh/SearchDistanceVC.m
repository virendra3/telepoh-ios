//
//  SearchDistanceVC.m
//  Telepoh
//
//  Created by apple on 20/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "SearchDistanceVC.h"

@interface SearchDistanceVC ()

{
    NSString * strDistanceMeter;
}

@end

@implementation SearchDistanceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    vwBackGround.layer.cornerRadius = 10.0;
    vwBackGround.clipsToBounds = YES;
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"]) {
            sliderView.value = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"] floatValue];
        }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"]) {
        strDistanceMeter = [[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"];
    }else{
        strDistanceMeter = @"Mile";
    }
    
    lblMinDistance.text = [NSString stringWithFormat:@"%.2f %@", sliderView.value,strDistanceMeter];
    lblMile.text = [NSString stringWithFormat:@"10.00 %@",strDistanceMeter];
    if (sliderView.value == 0.0) {
        lblMinDistance.text = [NSString stringWithFormat:@"500 Meter"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sliderAction:(id)sender {
    lblMinDistance.text = [NSString stringWithFormat:@"%.2f %@", sliderView.value,strDistanceMeter];
    if (sliderView.value == 0.0) {
        lblMinDistance.text = [NSString stringWithFormat:@"500 Meter"];
    }
    
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%.2f Mile", sliderView.value] forKey:@"SearchDistance"];
    
    
}

- (IBAction)btnTappedCancel:(id)sender {
    UIButton * btn = sender ;
    if (btn.tag == 1) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"UpdateNotification"
         object:self];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
