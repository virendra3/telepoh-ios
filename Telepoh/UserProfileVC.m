//
//  UserProfileVC.m
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "UserProfileVC.h"
#import "UserImageVC.h"
#import "UserReviewCustomCell.h"

@interface UserProfileVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    
    NSDictionary * dictInfoDetails;
    NSMutableArray * arrPersonalUserRating;
    NSMutableArray * arrProfilebarRating;

}

@end

@implementation UserProfileVC
@synthesize dictInfoUser;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Create Circular Profile Picture
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    arrPersonalUserRating = [NSMutableArray array];
    arrProfilebarRating = [NSMutableArray array];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    profileImg.layer.cornerRadius = profileImg.frame.size.width / 2;
    profileImg.clipsToBounds = YES;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    [profileImg setUserInteractionEnabled:YES];
    
    [profileImg addGestureRecognizer:tapGesture1];
    
    vwAvgRating.starImage =[UIImage imageNamed:@"star"] ;
    vwAvgRating.starHighlightedImage = [UIImage imageNamed:@"starActive"] ;
    vwAvgRating.maxRating = 5.0;
    vwAvgRating.editable=NO;
    vwAvgRating.rating= 2.5;
    vwAvgRating.displayMode=EDStarRatingDisplayHalf;
    NSLog(@"%@",dictInfoUser);
    [self userInfoDetails];
    
}



-(void)userInfoDetails{
    NSString *url=[NSString stringWithFormat:ShowUserRatingsURL];
    int userID ;
    if ([dictInfoUser valueForKey:@"ID"] ) {
        userID = [[dictInfoUser valueForKey:@"ID"] intValue];
    }else{
        userID = [[dictInfoUser valueForKey:@"UserDetailsID"] intValue];
    }
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:userID]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            dictInfoDetails = [[receivedData valueForKey:@"data"] mutableCopy];
            [self updateUserDetails];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP9" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}

-(void)updateUserDetails{
    profileName.text = [dictInfoDetails valueForKey:@"UserName"];
    if ([dictInfoDetails valueForKey:@"work"] == [NSNull null]) {
         profilePost.text = @"";
    }else{
        profilePost.text = [dictInfoDetails valueForKey:@"work"];
    }
     NSString * strURLImg = [[dictInfoDetails valueForKey:@"ProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    AsyncImageView *imgView = (AsyncImageView *)profileImg;
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    arrPersonalUserRating = [dictInfoDetails valueForKey:@"PersonalUserRating"];
    
    if (arrPersonalUserRating.count == 0) {
        lblNoRecords.hidden = false;
    }else{
        lblNoRecords.hidden =true;
    }
    
    vwAvgRating.starImage =[UIImage imageNamed:@"star"] ;
    vwAvgRating.starHighlightedImage = [UIImage imageNamed:@"starActive"] ;
    vwAvgRating.maxRating = 5.0;
    vwAvgRating.editable=NO;
    vwAvgRating.rating= [[dictInfoDetails valueForKey:@"AverageRating"] floatValue];
    vwAvgRating.displayMode=EDStarRatingDisplayHalf;
    
    totalReview.text = [NSString stringWithFormat:@"Total:%@",[dictInfoDetails valueForKey:@"TotalRatingCount"]];
    lblAvgRating.text = [NSString stringWithFormat:@"%@",[dictInfoDetails valueForKey:@"AverageRating"]];
    
    arrProfilebarRating = [dictInfoDetails valueForKey:@"ProfilebarRating"];
    lblRating1.text = [NSString stringWithFormat:@"%@",[arrProfilebarRating[4] valueForKey:@"RatingCount"]];
    lblRating2.text = [NSString stringWithFormat:@"%@",[arrProfilebarRating[3] valueForKey:@"RatingCount"]];
    lblRating3.text = [NSString stringWithFormat:@"%@",[arrProfilebarRating[2] valueForKey:@"RatingCount"]];
    lblRating4.text = [NSString stringWithFormat:@"%@",[arrProfilebarRating[1] valueForKey:@"RatingCount"]];
    lblRating5.text = [NSString stringWithFormat:@"%@",[arrProfilebarRating[0] valueForKey:@"RatingCount"]];
    
    [tblView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//TableView DataSources and delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrPersonalUserRating.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UserReviewCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserReviewCustomCell"];
    
    
    
    cell.lblUserName.text =[arrPersonalUserRating[indexPath.row] valueForKey:@"UserName"];
    cell.lblUserReview.text =[arrPersonalUserRating[indexPath.row] valueForKey:@"ReviewGiven"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.height/2;
    cell.imgUser.clipsToBounds = YES;
    
    AsyncImageView *imgView = (AsyncImageView *)cell.imgUser;
    [imgView setImageURL:[NSURL URLWithString:[arrPersonalUserRating[indexPath.row] valueForKey:@"ProfilePic"]]];
    
    cell.userReviewRating.starImage =[UIImage imageNamed:@"star"];
    cell.userReviewRating.starHighlightedImage = [UIImage imageNamed:@"starActive"];
    cell.userReviewRating.maxRating = 5.0;
    cell.userReviewRating.editable=NO;
    cell.userReviewRating.rating= [[arrPersonalUserRating[indexPath.row] valueForKey:@"RatingGiven"] floatValue];
    cell.userReviewRating.displayMode=EDStarRatingDisplayHalf;
    cell.btnUserProfile.tag = indexPath.row;
    [cell.btnUserProfile addTarget:self action:@selector(btnTappedUserProfile:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

-(void)btnTappedUserProfile: (UIButton *)sender{
    dictInfoUser = arrPersonalUserRating[sender.tag] ;
    
    NSMutableDictionary * dict = [arrPersonalUserRating[sender.tag] mutableCopy];
//    [dict removeObjectForKey:@"ID"];
    //    [dict removeObjectForKey:@"UserDetailsID"];
    [dict setValue:[dict objectForKey:@"UserID"] forKey:@"UserDetailsID"];
    dictInfoUser = dict;
    [self userInfoDetails];
    
}

-(void)tapGesture:(UITapGestureRecognizer*)sender {
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserImageVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserImageVC"];
    dashboardViewController.dictInfo  = dictInfoDetails;
    [self.navigationController pushViewController:dashboardViewController animated:YES];
}


- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
     [self.revealViewController revealToggle:sender];
}
@end
