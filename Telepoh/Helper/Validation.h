//
//  Validation.h
//
//
//  Created by promatics on 6/16/14.
//  Copyright (c) 2014 com.promatics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject


+(id)validationManager;
-(BOOL)validateEmail:(NSString*)emailString;
-(BOOL)validatePassword:(NSString*)password;
-(BOOL)validateBlankField:(NSString*)string;
-(BOOL)validatePhoneNumber:(NSString*)phoneString;
-(BOOL)validateZipCode:(NSString*)zipcodeString;
-(BOOL)validateCharacters:(NSString*)string;
-(BOOL)validateNumericNumber:(NSString*)string;
@end
