//
//  PaddedUITextField.m
//  MyOffice
//
//  Created by Apple on 07/07/14.
//  Copyright (c) 2014 myOffice. All rights reserved.
//

#import "PaddedUITextField.h"

@implementation PaddedUITextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self intializer];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self intializer];
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self) {
        // Initialization code
        [self intializer];
    }
    return self;
}


-(void)intializer{
    self.tintColor = [UIColor blackColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//- (CGRect)leftViewRectForBounds:(CGRect)bounds
//{
//    //[super leftViewRectForBounds:bounds];
//    
//    
//    
//    return CGRectInset(bounds, 50, 0);
//}

-(void) drawPlaceholderInRect:(CGRect)rect  {
    
    if (self.placeholder)
    {
        
        // color of placeholder text
        UIColor *placeHolderTextColor = [UIColor whiteColor];
        
        CGSize drawSize = [self.placeholder sizeWithAttributes:[NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName]];
        CGRect drawRect = rect;
        
        // verticially align text
        drawRect.origin.y = (rect.size.height - drawSize.height) * 0.5;
        
        // set alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = self.textAlignment;
        
        // dictionary of attributes, font, paragraphstyle, and color
        NSDictionary *drawAttributes = @{NSFontAttributeName: self.font,
                                         NSParagraphStyleAttributeName : paragraphStyle,
                                         NSForegroundColorAttributeName : placeHolderTextColor};
        
        
        // draw
        [self.placeholder drawInRect:drawRect withAttributes:drawAttributes];
        
    }
    
}

//- (CGRect)textRectForBounds:(CGRect)bounds
//{
//    [super textRectForBounds:bounds];
//    bounds.size.width = bounds.size.width-10;
//    
//    return CGRectInset(bounds, 10, 10);
//    
//}
//-(CGRect)editingRectForBounds:(CGRect)bounds
//{
//    [super editingRectForBounds:bounds];
//    bounds.size.width = bounds.size.width-10;
//    return CGRectInset(bounds, 10, 10);
//}

@end
