//
//  DateConversions.h
//  DirectVet
//
//  Created by Apple on 08/08/14.
//  Copyright (c) 2014 DirectVet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateConversions : NSObject

+(id)sharedInstance;

-(NSDate *)convertDateFromString:(NSString *)stringDate time:(BOOL)withTime;
-(NSString *)convertStringFromDate:(NSDate *)date time:(BOOL)withTime;

-(NSString *)convertStringfromStringDate:(NSString *)stringDate time:(BOOL)withTime wtihTimeZone:(NSString *)timeZone;
-(NSString *)convertStringfromStringForUSFormatDate:(NSString *)stringDate time:(BOOL)withTime wtihTimeZone:(NSString *)timeZone;

-(NSDateComponents *)convertComponentsFromDate:(NSDate *)date;
-(NSDate *)convertDateFromComponent:(NSDateComponents *)component;

@end
