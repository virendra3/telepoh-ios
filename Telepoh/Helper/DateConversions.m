//
//  DateConversions.m
//  DirectVet
//
//  Created by Apple on 08/08/14.
//  Copyright (c) 2014 DirectVet. All rights reserved.
//

#import "DateConversions.h"

@implementation DateConversions

+(id)sharedInstance
{
    static DateConversions *dateConversions=nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,^{
        dateConversions=[[self alloc]init];
    });
    return dateConversions;
}

-(id)init
{
    if (self=[super init]) {
        
    }
    return self;
}

-(NSDate *)convertDateFromString:(NSString *)stringDate time:(BOOL)withTime{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_CA_POSIX"];
    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"PDT"]];
    if (withTime) {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }else{
       [dateFormat setDateFormat:@"yyyy-MM-dd"];
    }
    NSDate *date = [dateFormat dateFromString:stringDate];
    
    NSLog(@"formatter %@",dateFormat.timeZone);
    
    return date;
}




-(NSString *)convertStringFromDate:(NSDate *)date time:(BOOL)withTime{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    if (withTime) {
        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    }else{
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
    }
    NSString *stringDate = [dateFormat stringFromDate:date];
    
    return stringDate;
}



-(NSString *)convertStringfromStringDate:(NSString *)stringDate time:(BOOL)withTime wtihTimeZone:(NSString *)timeZone{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    if (withTime) {
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    }else{
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
    }
    NSDate *date = [dateFormat dateFromString:stringDate];
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    //[dateFormat1 setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    if (withTime) {
        [dateFormat1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }else{
        [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    }
    
    
    NSLog(@"%@",[dateFormat1 stringFromDate:date]);
    
    return [dateFormat1 stringFromDate:date];
    
    
    
    
    //return date;
}

-(NSString *)convertStringfromStringForUSFormatDate:(NSString *)stringDate time:(BOOL)withTime wtihTimeZone:(NSString *)timeZone{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_CA_POSIX"];
    if (withTime) {
        [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }else{
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
    }
    NSDate *date = [dateFormat dateFromString:stringDate];
    
    
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    
    if (withTime) {
        [dateFormat1 setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
    }else{
        [dateFormat1 setDateFormat:@"MM-dd-yyyy"];
    }
    
    
    NSLog(@"%@",[dateFormat1 stringFromDate:date]);
    
    return [dateFormat1 stringFromDate:date];
    
}


-(NSDateComponents *)convertComponentsFromDate:(NSDate *)date{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //[calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    
    
    NSDateComponents *component = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday fromDate:date];
    
    return component;
}

-(NSDate *)convertDateFromComponent:(NSDateComponents *)component{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //[calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    return [calendar dateFromComponents:component];
}

@end
