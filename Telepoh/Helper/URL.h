//
//  URL.h
//  MyOffice
//
//  Created by promatics on 7/4/14.
//  Copyright (c) 2014 myOffice. All rights reserved.
//



/*  response m_code
        0 => success
        1 => failure
 */

#ifndef MatchingGame
#define MatchingGame

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define placeholder_text_color_hexcode 0xC9C9C9
#define text_color_hexcode 0x000000

/* domain */
//#define baseUrl                         @"http://telepoh.shrigenesis.com/api/"
//#define baseUrl                         @"http://web.telepoh.com/api/"
//#define baseUrl                         @"http://telepoh1.shrigenesis.com/api/"

/* webservice url Dot Net Server*/
/*
#define loginURL                        @"%@App/Login?",baseUrl
#define forgetURL                       @"%@App/ForgetPassword",baseUrl
#define registrationURL                 @"%@App/Registration?",baseUrl
#define changePasswordURL               @"%@App/ChangePassword?",baseUrl
#define addEventURL                     @"%@App/AddEvent1?",baseUrl
#define CheckLoginURL                   @"%@App/CheckLogin",baseUrl
#define eventDataURL                    @"%@App2/EventData",baseUrl
#define AcceptEventURL                  @"%@EventExecute/AcceptEvent",baseUrl
#define IgnoreEventURL                  @"%@EventIgnore/IgnoreEvent",baseUrl
#define MyPostedActivities              @"%@PostedActivites/MyPostedActivities",baseUrl
#define JoinedActivityUsersURL          @"%@PostedActivites/JoinedActivityUsers",baseUrl
#define JoinedUsersActivityURL          @"%@JoinedActivities/JoinedActivities",baseUrl
#define NotificationDataURL             @"%@ShowNotification/NotificationData",baseUrl

#define AcceptRequestURL                @"%@AcceptRequest/Accept",baseUrl
#define RejectRequestURL                @"%@RejectRequest/Reject",baseUrl

#define AllUserMessagesURL              @"%@AllUserMessages/UserMessages",baseUrl
#define ShowAllMessageURL               @"%@ShowAllMessage/ShowMessages",baseUrl
#define SendMessageURL                  @"%@SendMessage/ShowMessages",baseUrl

#define RatingsAddURL                   @"%@RatingsAdd/SaveRatings",baseUrl
#define ShowUserRatingsURL              @"%@Ratings/ShowUserRatings",baseUrl
#define UserProfilePicURL               @"%@UserProfilePic/DelinkUser",baseUrl
#define EditProfileUserDataURL          @"%@User/EditProfile",baseUrl
#define SaveImageURL                    @"%@App1/SaveImage",baseUrl
#define SaveProfileURL                  @"%@SaveUserData/SaveProfile",baseUrl

#define DelinkUserURL                   @"%@DetachUser/DelinkUser",baseUrl
#define LoginWithFBURL                  @"%@App/LoginWithFacebook",baseUrl
#define LoginWithGoogleURL              @"%@App/LoginWithGoogle",baseUrl

#define DeletedActivityURL              @"%@DeleteActivity/UserMessages",baseUrl
#define LogoutURL                       @"%@App/LogOut",baseUrl
#define CheckLogin1                     @"%@App/CheckLogin1",baseUrl
*/

/* webservice url Php Net Server*/
// #define baseUrl                         @"https://ibhumi.com/api/index.php/Api/"
#define baseUrl                         @"http://telepoh.thewebsitecart.com/api/index.php/Api/"
//#define baseUrl                         @"http://www.telepoh.bigperl.in/api/index.php/Api/"

 #define loginURL                        @"%@Login?",baseUrl
 #define forgetURL                       @"%@ForgetPassword",baseUrl
 #define registrationURL                 @"%@Registration?",baseUrl
 #define changePasswordURL               @"%@ChangePassword?",baseUrl
 #define addEventURL                     @"%@AddEvent1?",baseUrl
 #define CheckLoginURL                   @"%@CheckLogin",baseUrl
 #define eventDataURL                    @"%@EventData",baseUrl
 #define AcceptEventURL                  @"%@AcceptEvent",baseUrl
 #define IgnoreEventURL                  @"%@IgnoreEvent",baseUrl
 #define MyPostedActivities              @"%@MyPostedActivities",baseUrl
 #define JoinedActivityUsersURL          @"%@PostedActivites/JoinedActivityUsers",baseUrl
 #define JoinedUsersActivityURL          @"%@JoinedActivities",baseUrl
 #define NotificationDataURL             @"%@NotificationData",baseUrl
 
 #define AcceptRequestURL                @"%@Accept",baseUrl
 #define RejectRequestURL                @"%@Reject",baseUrl
 
 #define AllUserMessagesURL              @"%@UserMessages",baseUrl
 #define ShowAllMessageURL               @"%@ShowMessages",baseUrl
 #define SendMessageURL                  @"%@SendMessage",baseUrl
 
 #define RatingsAddURL                   @"%@SaveRatings",baseUrl
 #define ShowUserRatingsURL              @"%@ShowUserRatings",baseUrl
 #define UserProfilePicURL               @"%@UserProfilePic/DelinkUser",baseUrl
 #define EditProfileUserDataURL          @"%@EditProfile",baseUrl
 #define SaveImageURL                    @"%@SaveImage",baseUrl
 #define SaveProfileURL                  @"%@SaveProfile",baseUrl
 
 #define DelinkUserURL                   @"%@DelinkUser",baseUrl
 #define LoginWithFBURL                  @"%@LoginWithFaceBook",baseUrl
 #define LoginWithGoogleURL              @"%@LoginWithGoogle",baseUrl
 
 #define DeletedActivityURL              @"%@DeleteActivity",baseUrl
 #define LogoutURL                       @"%@LogOut",baseUrl
 #define CheckLogin1                     @"%@CheckLogin1",baseUrl


/* method type */

#pragma mark - method type

#define Post_Type             @"POST"
#define Get_type              @"GET"




#pragma mark - Parmeter type

#define email_Key           @"email"
#define password_Key        @"password"
#define ID_Key              @"ID"
#define Lattitude_Key       @"Lattitude"
#define Longitude_Key       @"Longitude"
#define UserDistence_Key    @"UserDistence"
#define LastID_key          @"LastID"
#define LastID1_Key         @"LastID1"


#endif
