//
//  GraphView.h
//  PitambarObjC
//
//  Created by admin on 20/04/16.
//  Copyright © 2016 Pitambar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphView : UIView
-(void)loaderFrame:(CGRect)rectUpper withLowerLayer:(CGRect)rectLower;
-(void)timeManage:(NSInteger)hour withMints:(NSInteger)mins withSeconds:(NSInteger )seconds;
-(void)startAnimating;
@end
