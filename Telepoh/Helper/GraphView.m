//
//  GraphView.m
//  PitambarObjC
//
//  Created by admin on 20/04/16.
//  Copyright © 2016 Pitambar. All rights reserved.
//

#import "GraphView.h"

@implementation GraphView

{
    CAShapeLayer *circleForLoader,*circleForLoader1,*circleForLoader2;
    NSTimer *timer;
    NSTimer *timer1;
    NSTimer *timer2;
    CGFloat transform,transform1,transform2;
    UILabel *lblHour;
    UILabel * lblMints;
    UILabel * lblSeconds;
}


- (void)drawRect:(CGRect)rect {
    
    
    
//    [self loaderFrame:CGRectMake(125, 45, 55, 55) withLowerLayer:CGRectMake(128, 49, 48, 48)];
    
    
    
}
-(void)timeManage:(NSInteger)hour withMints:(NSInteger)mins withSeconds:(NSInteger )seconds{
    
    lblHour = [[UILabel alloc] initWithFrame:CGRectMake(61, 1, 38, 37)];
    lblHour.text = @"60";
    lblHour.textColor = [UIColor redColor];
    lblHour.textAlignment = NSTextAlignmentCenter;
    lblHour.layer.masksToBounds = NO;
    [self addSubview:lblHour];
    
    lblMints = [[UILabel alloc] initWithFrame:CGRectMake(110, 1, 38, 37)];
    lblMints.text = @"15";
    lblMints.textColor = [UIColor redColor];
    lblMints.textAlignment = NSTextAlignmentCenter;
    lblMints.layer.masksToBounds = NO;
    [self addSubview:lblMints];
    
    lblSeconds = [[UILabel alloc] initWithFrame:CGRectMake(160, 1, 38, 37)];
    lblSeconds.text = @"10";
    lblSeconds.textColor = [UIColor redColor];
    lblSeconds.textAlignment = NSTextAlignmentCenter;
    lblSeconds.layer.masksToBounds = NO;
    [self addSubview:lblSeconds];
    
    timer = [NSTimer timerWithTimeInterval:0.0 target:self selector:@selector(rotateARC:) userInfo:@"0.0" repeats:NO];
    timer1 = [NSTimer timerWithTimeInterval:0.0 target:self selector:@selector(rotateSecondARC:) userInfo:@"0.0" repeats:NO];
    timer2 = [NSTimer timerWithTimeInterval:0.0 target:self selector:@selector(rotateThirdARC:) userInfo:@"0.0" repeats:NO];
    
    [self draw];
    
    lblHour.text = [NSString stringWithFormat:@"%ld",(long)hour];
    lblMints.text = [NSString stringWithFormat:@"%ld",(long)mins];
    lblSeconds.text = [NSString stringWithFormat:@"%ld",(long)seconds];
    
    [self startAnimating];
//    [self addSubview:lblHour];
}
-(void)loaderFrame:(CGRect)rectUpper withLowerLayer:(CGRect)rectLower{
    CAShapeLayer *circleLayerLower = [CAShapeLayer layer];
    [circleLayerLower setPath:[[UIBezierPath bezierPathWithOvalInRect:rectLower] CGPath]];
    
    [circleLayerLower setStrokeColor:[[UIColor grayColor] CGColor]];
    [circleLayerLower setFillColor:[[UIColor clearColor] CGColor]];
    [[self layer] addSublayer:circleLayerLower];
    
    CAShapeLayer *circleLayerUpper = [CAShapeLayer layer];
    [circleLayerUpper setPath:[[UIBezierPath bezierPathWithOvalInRect:rectUpper] CGPath]];
    
    [circleLayerUpper setStrokeColor:[[UIColor grayColor] CGColor]];
    [circleLayerUpper setFillColor:[[UIColor clearColor] CGColor]];
    [[self layer] addSublayer:circleLayerUpper];
    
}

-(CALayer *)addArcForView:(UIView *)viewForArc withIndentionInRadius:(CGFloat)x withAngleIndention:(CGFloat)indent clockwise:(int)clockwise
{
    CAShapeLayer *circle = [CAShapeLayer layer];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, CGRectGetMidX(viewForArc.bounds), CGRectGetMidY(viewForArc.bounds), CGRectGetMidY(viewForArc.bounds)-x,M_PI+indent,M_PI, clockwise);
    
    
    
    circle.fillColor = [[UIColor clearColor] CGColor];
    circle.strokeColor = [UIColor grayColor].CGColor;
    circle.bounds = CGRectMake(CGRectGetMidX(viewForArc.bounds), CGRectGetMidY(viewForArc.bounds), 0, 0);
    circle.path = path;
    circle.borderWidth = 25.0;
    circle.lineWidth = 5.0;
    circle.position = CGPointMake(CGRectGetMidX(viewForArc.bounds), CGRectGetMidY(viewForArc.bounds));
    
    return circle;
}

-(void)startAnimating{
//    NSTimer * time = [NSTimer timerWithTimeInterval:0.0 target:self selector:@selector(rotateARC:) userInfo:@"5.0" repeats:NO];
    
    
   
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    [[NSRunLoop mainRunLoop] addTimer:timer1 forMode:NSRunLoopCommonModes];
    [[NSRunLoop mainRunLoop] addTimer:timer2 forMode:NSRunLoopCommonModes];
    [self after60MintCompleted];
    
}


-(void)after60MintCompleted{
//    NSLog(@"One mint completed");
    
    
    
    int second = [lblSeconds.text intValue];
    if (second == 0) {
        second = 60;
        int mints = [lblMints.text intValue];
       
        
        if (mints == 0) {
            mints = 60;
            int hour = [lblHour.text intValue];
            
            hour = hour -1;
            lblHour.text =[NSString stringWithFormat:@"%d",hour];
        }
        mints = mints -1;
        lblMints.text =[NSString stringWithFormat:@"%d",mints];
    }
    second = second -1;
    lblSeconds.text =[NSString stringWithFormat:@"%d",second];
    
    [self performSelector:@selector(after60MintCompleted) withObject:nil afterDelay:1.0];
}
-(void)stopAnimating{
    
    [timer invalidate];
}


-(void)rotateARC:(CGFloat)time
{
    NSLog(@"%@", (NSString*)[timer userInfo]);
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.duration = 3600.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10001;
    
    CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    rotationAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    
    [circleForLoader addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}

-(void)rotateSecondARC:(CGFloat)time
{
    NSLog(@"%@", (NSString*)[timer1 userInfo]);
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.duration = 9.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10001;
    
    CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    rotationAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    
    [circleForLoader1 addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}

-(void)rotateThirdARC:(CGFloat)time
{
    NSLog(@"%@", (NSString*)[timer2 userInfo]);
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.duration = 0.1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 10001;
    
    CATransform3D xform = CATransform3DIdentity;
    xform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    rotationAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    
    [circleForLoader2 addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}

-(void)draw{
    circleForLoader = (CAShapeLayer *)[self addArcForView:lblHour withIndentionInRadius:.5 withAngleIndention:.8 clockwise:1];
    [lblHour.layer addSublayer:circleForLoader];
    circleForLoader1 = (CAShapeLayer *)[self addArcForView:lblMints withIndentionInRadius:.5 withAngleIndention:.8 clockwise:1];
    [lblMints.layer addSublayer:circleForLoader1];
    
    circleForLoader2 = (CAShapeLayer *)[self addArcForView:lblSeconds withIndentionInRadius:.5 withAngleIndention:.8 clockwise:1];
    [lblSeconds.layer addSublayer:circleForLoader2];
}

@end