//
//  UserReviewCustomCell.h
//  Telepoh
//
//  Created by apple on 21/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface UserReviewCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet EDStarRating *userReviewRating;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserReview;
@property (weak, nonatomic) IBOutlet UIButton *btnUserProfile;

@end
