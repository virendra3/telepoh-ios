//
//  ChatMessageCell.h
//  Telepoh
//
//  Created by apple on 23/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIView *vcBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end
