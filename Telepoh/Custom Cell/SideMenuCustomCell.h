//
//  SideMenuCustomCell.h
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblActionName;
@property (weak, nonatomic) IBOutlet UIImageView *imgAction;

@end
