//
//  InboxCustomCell.h
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface InboxCustomCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *btnRateStar;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnStartChat;
@property (weak, nonatomic) IBOutlet UIButton *btnUserProfile;

@property (weak, nonatomic) IBOutlet UIView *vwStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblUserDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@end
