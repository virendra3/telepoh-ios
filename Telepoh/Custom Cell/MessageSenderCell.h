//
//  MessageSenderCell.h
//  Telepoh
//
//  Created by apple on 07/08/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageSenderCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIView *vcBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end
