//
//  UserImageVC.m
//  Telepoh
//
//  Created by apple on 21/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "UserImageVC.h"

@interface UserImageVC ()
{
    NSArray* imageArray;
    UIImageView * imageView;
    IBOutlet UIPageControl *pageController;
    IBOutlet UIView *vwBackGround;
    
    Indicator * indicator;
    WebServiceConnection * newConnection;
    UIScrollView  *scrollView;
}

@end

@implementation UserImageVC
@synthesize dictInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    if ([dictInfo valueForKey:@"work"] == [NSNull null]) {
        UserPost.text = @"";
    }else{
        UserPost.text = [dictInfo valueForKey:@"work"];
    }
//    UserPost.text = [dictInfo valueForKey:@"work"];
    lblUserName.text = [dictInfo valueForKey:@"UserName"];
    [self UserProfilePic];
    
    
    
    
    
    btnAbout.layer.cornerRadius = 10;
    btnAbout.layer.borderColor =[UIColor whiteColor].CGColor;
    btnAbout.layer.borderWidth = 1.0;
    btnAbout.clipsToBounds = YES;
    
    
   
}

-(void)UserProfilePic{
    NSString *url=[NSString stringWithFormat:UserProfilePicURL];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[dictInfo valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            UserAge.text = [NSString stringWithFormat:@"(%@ year old)",[receivedData valueForKey:@"BirthTime"]];
            imageArray = [receivedData valueForKey:@"data"];
            [self imageScrollViewCreate];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP8" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}

-(void)imageScrollViewCreate{
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGFloat frameX = size.width;
    CGFloat frameY = size.height-64; //padding for UIpageControl
    
    scrollView  = [[UIScrollView alloc] initWithFrame:CGRectMake(0 , 0, frameX, frameY)];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake(frameX, frameY);
    
    vwBackGround.backgroundColor = [UIColor blackColor];
    
    
    for(int i = 0; i <imageArray.count ; i++)
    {
        
        
//        UIImage *image = [UIImage imageNamed:[imageArray objectAtIndex:i]];
        imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake(frameX * i, 0.0, frameX, frameY);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        imageView.image = [UIImage imageNamed:@"default.jpg"];
        
        AsyncImageView *imgView = (AsyncImageView *)imageView;
        
        
        NSString * strURLImg =  [[imageArray[i] valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgView setImageURL:[NSURL URLWithString:strURLImg]];
        [scrollView addSubview:imageView];
    }
    [vwBackGround addSubview: scrollView];
    scrollView.contentSize = CGSizeMake(frameX*[imageArray count], frameY);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float width = scrollView.frame.size.width;
    float xPos = scrollView.contentOffset.x+10;
    
    //Calculate the page we are on based on x coordinate position and width of scroll view
    pageController.currentPage = (int)xPos/width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnClickedAbout:(id)sender{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnTappedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
