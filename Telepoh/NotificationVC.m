//
//  NotificationVC.m
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "NotificationVC.h"
#import "InboxCustomCell.h"
#import "ReviewVC.h"
#import "InboxVC.h"
#import "UserProfileVC.h"
@interface NotificationVC ()
{
    NSMutableArray * arrUserList;
    Indicator * indicator;
    WebServiceConnection * newConnection;
    
    IBOutlet UILabel *lblNoRecords;
    NSInteger selectedSubmitIndex;
    
}
@property (strong,nonatomic)  NSString * strURLImg;
@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    arrUserList = [NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(submitReview:)
                                                 name:@"SubmitReview"
                                               object:nil];

}

-(void)viewWillAppear:(BOOL)animated{
    [self NotificationDataList];
}

- (void) submitReview:(NSNotification *) notification{
    [self NotificationDataList];
}

-(void)NotificationDataList{
    NSString *url=[NSString stringWithFormat:NotificationDataURL];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrUserList = [[receivedData valueForKey:@"data"] mutableCopy];
            [tblView reloadData];
        }else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP3" otherButtonTitles:nil, nil];
//            [alert show];
        }
        
        if (arrUserList.count == 0) {
            lblNoRecords.hidden = false;
            
        }else{
            lblNoRecords.hidden =true;
        }
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrUserList.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InboxCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCell"];
    InboxCustomCell *cellSubmit = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCellSubmit"];
    InboxCustomCell *cellStartChat = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCellStartChat"];
    
    if ([[arrUserList[indexPath.row] valueForKey:@"Status"] isEqualToString:@"Request"]) {
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.userImage setImageURL:[NSURL URLWithString:self.strURLImg]];
        NSLog(@"%@",self.strURLImg);
        cell.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
        cell.userImage.clipsToBounds = YES;
        
        AsyncImageView *imgView = (AsyncImageView *)cell.userImage;
        
        self.strURLImg = [[arrUserList[indexPath.row] valueForKey:@"UserProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgView setImageURL:[NSURL URLWithString:self.strURLImg]];
        
        cell.btnAccept.layer.cornerRadius = 10.0;
        cell.btnAccept.clipsToBounds = YES;
        
        cell.btnReject.layer.cornerRadius = 10.0;
        cell.btnReject.layer.borderColor = [UIColor grayColor].CGColor;
        cell.btnReject.layer.borderWidth = 1.0;
        cell.btnReject.clipsToBounds = YES;
        
        cell.lblUserName.text = [NSString stringWithFormat:@"%@ Wants to join your activity",[arrUserList[indexPath.row] valueForKey:@"OtherUserName"]];
        cell.btnAccept.tag = indexPath.row;
        cell.btnReject.tag = indexPath.row;
        [cell.btnAccept addTarget:self action:@selector(btnTappedAccept:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnReject addTarget:self action:@selector(btnTappedReject:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }else if ([[arrUserList[indexPath.row] valueForKey:@"Status"] isEqualToString:@"Complete"])  {
        [cellSubmit setSelectionStyle:UITableViewCellSelectionStyleNone];
       // [imgView setImageURL:[NSURL URLWithString:strURLImg]];
        [cellSubmit.userImage setImageURL:[NSURL URLWithString:self.strURLImg ]];
        cellSubmit.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
        cellSubmit.userImage.clipsToBounds = YES;
        
        cellSubmit.lblUserName.text = [NSString stringWithFormat:@"%@",[arrUserList[indexPath.row] valueForKey:@"OtherUserName"]];
        
        cellSubmit.btnSubmit.layer.cornerRadius = 10.0;
        cellSubmit.btnSubmit.clipsToBounds = YES;
        
        cellSubmit.starRating.starImage =[UIImage imageNamed:@"star"];
        cellSubmit.starRating.starHighlightedImage = [UIImage imageNamed:@"starActive"];
        cellSubmit.starRating.maxRating = 5.0;
        cellSubmit.starRating.editable=NO;
        cellSubmit.starRating.rating= 0.0;
        cellSubmit.starRating.displayMode=EDStarRatingDisplayHalf;
        cellSubmit.btnSubmit.tag = indexPath.row;
         [cellSubmit.btnSubmit addTarget:self action:@selector(btnTappedSubmit:) forControlEvents:UIControlEventTouchUpInside];
        AsyncImageView *imgView = (AsyncImageView *)cellSubmit.userImage;
        
        self.strURLImg = [[arrUserList[indexPath.row] valueForKey:@"UserProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgView setImageURL:[NSURL URLWithString:self.strURLImg]];
        
        cellSubmit.btnRateStar.tag = indexPath.row;
        [cellSubmit.btnRateStar addTarget:self action:@selector(btnTappedSubmit:) forControlEvents:UIControlEventTouchUpInside];

        return cellSubmit;
    }else  {
        [cellStartChat setSelectionStyle:UITableViewCellSelectionStyleNone];
         [cellStartChat.userImage setImageURL:[NSURL URLWithString:self.strURLImg ]];
        cellStartChat.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
        cellStartChat.btnStartChat.tag = indexPath.row;
        [cellStartChat.btnStartChat addTarget:self action:@selector(btnTappedStartChat:) forControlEvents:UIControlEventTouchUpInside];
        cellStartChat.btnStartChat.layer.cornerRadius = 10.0;
        cellStartChat.btnStartChat.clipsToBounds = YES;
        cellStartChat.lblUserName.text = [NSString stringWithFormat:@"%@",[arrUserList[indexPath.row] valueForKey:@"OtherUserName"]];
        cellStartChat.userImage.clipsToBounds = YES;
        AsyncImageView *imgView = (AsyncImageView *)cellStartChat.userImage;
        
        self.strURLImg = [[arrUserList[indexPath.row] valueForKey:@"UserProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgView setImageURL:[NSURL URLWithString:self.strURLImg]];

        return cellStartChat;
    }
    
    
   
    //    cell.lblUserName.text =arrMenuLbl[indexPath.row];
    //    cell.lblUserDescription.text =arrMenuLbl[indexPath.row];
    //    cell.userImage.image = [UIImage imageNamed:arrImage[indexPath.row]];
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    NSMutableDictionary * dict = [arrUserList[indexPath.row] mutableCopy];
    [dict removeObjectForKey:@"ID"];
//    [dict removeObjectForKey:@"UserDetailsID"];
    [dict setValue:[dict objectForKey:@"OtherUserID"] forKey:@"UserDetailsID"];
    dashboardViewController.dictInfoUser = dict;
    [self.navigationController pushViewController:dashboardViewController animated:NO];
    
}
-(void)btnTappedAccept:(UIButton *)sender{
    UIButton * btn = sender;
    NSLog(@"Accept:%ld",(long)btn.tag);
    
    NSString *url=[NSString stringWithFormat:AcceptRequestURL];
    
    [self acceptRejectNotification:url withId:[[arrUserList[btn.tag] valueForKey:@"ID"] intValue] withType:@"iOS"];
}
-(void)btnTappedReject:(UIButton *)sender{
    UIButton * btn = sender;
    NSLog(@"Reject:%ld",(long)btn.tag);
    NSString *url=[NSString stringWithFormat:RejectRequestURL];
    
    [self acceptRejectNotification:url withId:[[arrUserList[btn.tag] valueForKey:@"ID"] intValue] withType:@""];
}
-(void)btnTappedSubmit:(UIButton *)sender{
    UIButton * btn = sender;
    NSLog(@"Submit:%ld",(long)btn.tag);
    selectedSubmitIndex = btn.tag;
    
    [self performSegueWithIdentifier:@"addReview" sender:self];
    
//    ReviewVC *mainViewController=[[ReviewVC alloc]init];
//    mainViewController.dictInfoEvent = arrUserList[btn.tag];
//    [self presentViewController:mainViewController animated:YES completion:nil];
}
-(void)btnTappedStartChat:(UIButton *)sender{
    UIButton * btn = sender;
    NSLog(@"Start Chat:%ld",(long)btn.tag);
    NSDictionary *urlParam;
    NSString *url=[NSString stringWithFormat:CheckLogin1];
    urlParam=@{ID_Key:[NSNumber numberWithInt:[[arrUserList[btn.tag] valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            UIStoryboard *storyboard;
            
            
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            InboxVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"InboxVC"];
            dashboardViewController.strLastController = @"Menu";
            [self.navigationController pushViewController:dashboardViewController animated:NO];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP4" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];

    
   
//    [self.revealViewController revealToggleAnimated:YES];
}

-(void)acceptRejectNotification:(NSString *)url withId:(int)notificationID withType:(NSString *)strType{
    NSDictionary *urlParam;
    if ([strType isEqualToString:@"iOS"]) {
        urlParam=@{ID_Key:[NSNumber numberWithInt:notificationID],@"Type":@"iOS"};
    }else{
        urlParam=@{ID_Key:[NSNumber numberWithInt:notificationID]};
    }
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
//        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
//            [arrUserList removeObjectAtIndex:<#(NSUInteger)#>];
//            [tblView reloadData];
            [self NotificationDataList];
//        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP5" otherButtonTitles:nil, nil];
            [alert show];
//        }
        
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ReviewVC *mainViewController=[segue destinationViewController];
    mainViewController.dictInfoEvent = arrUserList[selectedSubmitIndex];
    
}
- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}
@end
