//
//  ChangePassword.m
//  Telepoh
//
//  Created by apple on 10/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "ChangePassword.h"

@interface ChangePassword ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    Validation * validateObj;
}

@end

@implementation ChangePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    validateObj = [Validation validationManager];
    
    vwChangePassword.layer.shadowRadius = 10.0;
    vwChangePassword.layer.cornerRadius = 10.0;
    vwChangePassword.clipsToBounds = YES;
    
    btnChangePassword.layer.cornerRadius = 10.0;
    btnChangePassword.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}

- (IBAction)btnTappedChangePassword:(id)sender {
    NSString *message = nil;
    
    if (![validateObj validateBlankField:[txtfield_OldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Old Password";
    }
    else if (![validateObj validateBlankField:[txtField_NewPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter New Password";
    }
    else if (![validateObj validateBlankField:[txtFieldConfirmPasswrd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Confirm Password";
    }
    else if (![txtField_NewPassword.text isEqualToString:txtFieldConfirmPasswrd.text])
    {
        message =  @"Wrong Confirm Password";
    }
    
    if ([message length]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else{
        [self changePasswordWebservice];
    }
}

#pragma mark - WebService changePassword method
-(void)changePasswordWebservice
{
    
    NSString * strURL =[NSString stringWithFormat:changePasswordURL];
    
    strURL = [strURL stringByAppendingString:[NSString stringWithFormat:@"id=%@&NewPassword=%@&OldPassword=%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"],txtField_NewPassword.text,txtfield_OldPassword.text]];
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:strURL HttpMethodType:Get_type HttpBodyType:nil Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        txtField_NewPassword.text = @"";
        txtfield_OldPassword.text = @"";
        txtFieldConfirmPasswrd.text = @"";
        
        
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        
    }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
@end
