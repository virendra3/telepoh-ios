//
//  EditProfileVC.m
//  Telepoh
//
//  Created by apple on 15/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//


#import "EditProfileVC.h"

#import "DateConversions.h"

#import "HomeVC.h"



@interface EditProfileVC (){
    
    NSString * strMeterMode;
    
    UIDatePicker *datePicker;
    
    DateConversions *dateConversion;
    
    Indicator * indicator;
    
    WebServiceConnection * newConnection;
    
    Validation * validateObj;
    
    
    
    
    
    BOOL isImageFirst;
    
    BOOL isImageSecond;
    
    BOOL isImageThird;
    
    BOOL isImageFour;
    
    BOOL isImageFive;
    
    
    
    IBOutlet UILabel *lblDistanceMinValue;
    
    NSString * strGender;
    
    NSString * strInterest;
    
    NSString * strDistance;
    
    NSMutableArray * arrProfilePic;
    
    NSMutableArray * arrUploadedImage;
    
}


@end


@implementation EditProfileVC


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
   // indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    
    newConnection=[WebServiceConnection connectinManager];
    
    validateObj = [Validation validationManager];
    
    
    
    dateConversion = [DateConversions sharedInstance];
    
    arrProfilePic = [NSMutableArray array];
    
    strGender = @"Male";
    
    strInterest = @"Male";
    
    strDistance = @"0.0";
    
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"]) {
        
        slider.value = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"] floatValue];
        
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"]) {
        
        strMeterMode = [[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"];
        
        if ([strMeterMode isEqualToString:@"Km"]) {
            
            [btnKilometer setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
            
            [btnMiles setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
            
            strMeterMode = @"Km";
            
            
            
            
            
        }else{
            
            
            
            [btnMiles setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
            
            [btnKilometer setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
            
            strMeterMode = @"Mile";
            
            
            
            
            
        }
        
    }else{
        
        strMeterMode = @"Km";
        
    }
    
    
    
    lblSearchMeter.text = [NSString stringWithFormat:@"10.0 %@", strMeterMode];
    
    lblDistanceMinValue.text = [NSString stringWithFormat:@"%.2f %@", slider.value,strMeterMode];
    
    if (slider.value == 0.0) {
        
        lblDistanceMinValue.text = @"500 meter";
        
    }
    
    
    
    
    
    [txtFieldTitle becomeFirstResponder];
    
    [self userImageRound];
    
    [self imageGesture];
    
    [self userProfileData];
    
}


-(void)userProfileData{
    
    
    
    NSString *url=[NSString stringWithFormat:EditProfileUserDataURL];
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        
        NSLog(@"%@",receivedData);
        
        [indicator removeFromSuperview];
        
        
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            
            if ([[receivedData valueForKey:@"data"] valueForKey:@"Country"] == [NSNull null]) {
                
                txtFieldCity.text = @"";
                
            }else{
                
                txtFieldCity.text = [[receivedData valueForKey:@"data"] valueForKey:@"City"];
                
            }
            
            if ([[receivedData valueForKey:@"data"] valueForKey:@"City"] == [NSNull null]) {
                
                txtFieldCountry.text = @"";
                
            }else{
                
                txtFieldCountry.text = [[receivedData valueForKey:@"data"] valueForKey:@"Country"];
                
            }
            
            txtFieldTitle.text = [[receivedData valueForKey:@"data"] valueForKey:@"UserName"];
            
            txtFieldAboutMe.text = [[receivedData valueForKey:@"data"] valueForKey:@"AboutMe"];
            
            
            
            
            
            txtField_DOB.text = [[receivedData valueForKey:@"data"] valueForKey:@"DOB1"];
            
            txtFieldWork.text = [[receivedData valueForKey:@"data"] valueForKey:@"Work"];
            
            
            
            NSMutableArray * arrImages = [[receivedData valueForKey:@"data"] valueForKey:@"ProfilePicDetails"];
            
            int counter = 0;
            
            for (NSDictionary * dict in arrImages) {
                
                
                
                NSString * strURLImg = [[dict valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                
                if (counter == 0) {
                    
                    AsyncImageView *imgView = (AsyncImageView *)imgUser1;
                    
                    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
                    
                }else if (counter == 1) {
                    
                    AsyncImageView *imgView = (AsyncImageView *)imgUser2;
                    
                    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
                    
                }else if (counter == 2) {
                    
                    AsyncImageView *imgView = (AsyncImageView *)imgUser3;
                    
                    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
                    
                }else if (counter == 3) {
                    
                    AsyncImageView *imgView = (AsyncImageView *)imgUser4;
                    
                    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
                    
                }else if (counter == 4) {
                    
                    AsyncImageView *imgView = (AsyncImageView *)imgUser5;
                    
                    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
                    
                }
                
                [arrProfilePic addObject:[dict valueForKey:@"ID"]];
                
                
                
                counter = counter+1;
                
                
                
            }
            
            if ([[receivedData valueForKey:@"data"] valueForKey:@"Gender"] == [NSNull null]) {
                
                [btnMale setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                [btnFemale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
            }else if ([[[receivedData valueForKey:@"data"] valueForKey:@"Gender"] isEqualToString:@"Male"]) {
                
                [btnMale setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                [btnFemale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                
                
            }else{
                
                [btnFemale setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                [btnMale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                
                
            }
            
            
            
            if ([[[receivedData valueForKey:@"data"] valueForKey:@"Intrest"] isEqualToString:@""]) {
                
                [btnMaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                [btnFemaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                [btnBothInterest setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                
                
            }else if([[[receivedData valueForKey:@"data"] valueForKey:@"Intrest"] isEqualToString:@"Male"]){
                
                [btnMaleInterest setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                [btnFemaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                [btnBothInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                
                
            }else if([[[receivedData valueForKey:@"data"] valueForKey:@"Intrest"] isEqualToString:@"Female"]){
                
                [btnFemaleInterest setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
                
                [btnMaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                [btnBothInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
                
                
                
            }
            
            
            
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        
        
        
    }];
    
}



-(void)imageGesture{
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [imgUser1 setUserInteractionEnabled:YES];
    
    
    
    [imgUser1 addGestureRecognizer:tapGesture1];
    
    
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture2:)];
    
    
    
    tapGesture2.numberOfTapsRequired = 1;
    
    [imgUser2 setUserInteractionEnabled:YES];
    
    
    
    [imgUser2 addGestureRecognizer:tapGesture2];
    
    
    
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture3:)];
    
    
    
    tapGesture3.numberOfTapsRequired = 1;
    
    [imgUser3 setUserInteractionEnabled:YES];
    
    
    
    [imgUser3 addGestureRecognizer:tapGesture3];
    
    
    
    UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture4:)];
    
    
    
    tapGesture4.numberOfTapsRequired = 1;
    
    [imgUser4 setUserInteractionEnabled:YES];
    
    
    
    [imgUser4 addGestureRecognizer:tapGesture4];
    
    
    
    UITapGestureRecognizer *tapGesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture5:)];
    
    
    
    tapGesture5.numberOfTapsRequired = 1;
    
    [imgUser5 setUserInteractionEnabled:YES];
    
    
    
    [imgUser5 addGestureRecognizer:tapGesture5];
    
    
}

- (void) tapGesture: (UITapGestureRecognizer *)tap

{
    
    isImageFirst = true;
    
    isImageSecond = false;
    
    isImageThird = false;
    
    isImageFour = false;
    
    isImageFive = false;
    
    [self SetProfileImage];
    
}


- (void) tapGesture2: (UITapGestureRecognizer *)tap

{
    
    isImageFirst = false;
    
    isImageSecond = true;
    
    isImageThird = false;
    
    isImageFour = false;
    
    isImageFive = false;
    
    [self SetProfileImage];
    
}


- (void) tapGesture3: (UITapGestureRecognizer *)tap

{
    
    isImageFirst = false;
    
    isImageSecond = false;
    
    isImageThird = true;
    
    isImageFour = false;
    
    isImageFive = false;
    
    [self SetProfileImage];
    
}


- (void) tapGesture4: (UITapGestureRecognizer *)tap

{
    
    isImageFirst = false;
    
    isImageSecond = false;
    
    isImageThird = false;
    
    isImageFour = true;
    
    isImageFive = false;
    
    [self SetProfileImage];
    
}


- (void) tapGesture5: (UITapGestureRecognizer *)tap

{
    
    isImageFirst = false;
    
    isImageSecond = false;
    
    isImageThird = false;
    
    isImageFour = false;
    
    isImageFive = true;
    
    [self SetProfileImage];
    
}

-(void)userImageRound{
    
    
    imgUser1.layer.cornerRadius = 22.0;
    
    imgUser1.clipsToBounds = YES;
    
    
    
    imgUser2.layer.cornerRadius = 22.0;
    
    imgUser2.clipsToBounds = YES;
    
    
    
    imgUser3.layer.cornerRadius = 22.0;
    
    imgUser3.clipsToBounds = YES;
    
    
    
    imgUser4.layer.cornerRadius = 22.0;
    
    imgUser4.clipsToBounds = YES;
    
    
    
    imgUser5.layer.cornerRadius = 22.0;
    
    imgUser5.clipsToBounds = YES;
    
    
    
    strMeterMode = @"Mile";
    
    
    
    
    
    datePicker = [[UIDatePicker alloc]init];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [datePicker setMaximumDate:[NSDate date]];
    
    [datePicker addTarget:self action:@selector(setPickerDate:) forControlEvents:UIControlEventValueChanged];
    
    [txtField_DOB setInputView:datePicker];
    
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(cancelKeyboard:)];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    toolbar.items = @[cancelButton];
    
    txtField_DOB.inputAccessoryView = toolbar;
    
}


-(void)cancelKeyboard:(UIBarButtonItem *)sender{
    
    [txtField_DOB resignFirstResponder];
    
    txtField_DOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
    
    NSLog(@"%@",txtField_DOB.text);
    
}


-(void)setPickerDate:(UIDatePicker *)picker{
    
    txtField_DOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
    
    NSLog(@"%@",txtField_DOB.text);
    
    
    
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}


/*
 
 #pragma mark - Navigation
 
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 // Get the new view controller using [segue destinationViewController].
 
 // Pass the selected object to the new view controller.
 
 }
 
 */


- (IBAction)btnTappedGender:(id)sender {
    
    UIButton * btn  = sender;
    
    
    
    if (btn.tag == 1) {
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnFemale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strGender = @"Male";
        
        
        
        
        
    }else{
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnMale setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strGender = @"Female";
        
        
        
    }
    
}


- (IBAction)btnTappedDistanceUnit:(id)sender {
    
    UIButton * btn  = sender;
    
    
    
    if (btn.tag == 1) {
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnKilometer setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strMeterMode = @"Mile";
        
        
        
    }else{
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnMiles setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strMeterMode = @"Km";
        
        
        
        
        
    }
    
    
    
    lblSearchMeter.text = [NSString stringWithFormat:@"10.0 %@",strMeterMode];
    
    
    
    lblSearchMeter.text = [NSString stringWithFormat:@"10.0 %@", strMeterMode];
    
    lblDistanceMinValue.text = [NSString stringWithFormat:@"%.2f %@", slider.value,strMeterMode];
    
    if (slider.value == 0.0) {
        
        lblDistanceMinValue.text = @"500 meter";
        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults] setValue:strMeterMode forKey:@"SearchDistanceType"];
    
    //    lblDistanceMinValue.text = [NSString stringWithFormat:@"500 %@",strMeterMode];
    
}


- (IBAction)btnTappedInterest:(id)sender {
    
    UIButton * btn  = sender;
    
    
    
    if (btn.tag == 1) {
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnFemaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        [btnBothInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strInterest = @"Male";
        
        
        
        
        
    }else if(btn.tag == 2){
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnMaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        [btnBothInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strInterest = @"Female";
        
        
        
    }else {
        
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        
        [btnFemaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        [btnMaleInterest setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        
        strInterest = @"Both";
        
        
        
    }
    
}


- (IBAction)sliderValueChange:(id)sender {
    
    lblSearchMeter.text = [NSString stringWithFormat:@"10.0 %@", strMeterMode];
    
    lblDistanceMinValue.text = [NSString stringWithFormat:@"%.2f %@", slider.value,strMeterMode];
    
    if (slider.value == 0.0) {
        
        lblDistanceMinValue.text = @"500 meter";
        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%.2f Mile", slider.value] forKey:@"SearchDistance"];
    
}


- (IBAction)btnTappedSave:(id)sender {
    
    [self updateUserProfile];
    
}




-(void)updateUserProfile{
    
    
    
    NSString * strPic = [arrProfilePic componentsJoinedByString:@","];
    
    
    
    NSString *url=[NSString stringWithFormat:SaveProfileURL];
    
    
    
    NSString * strAboutMe =@"";
    
    if (txtFieldAboutMe.text.length <= 0 || txtFieldAboutMe.text != nil || ![txtFieldAboutMe.text isEqual:@""])
        
    {
        
        //do your work
        
        
        
        strAboutMe = txtFieldAboutMe.text;
        
    }else{
        
        strAboutMe = @"";
        
    }
    
    
    
    
    
    NSString * strCity =@"";
    
    if (txtFieldCity.text.length > 0 || txtFieldCity.text != nil || ![txtFieldCity.text isEqual:@""])
        
    {
        
        //do your work
        
        
        
        strCity = @"";
        
    }else{
        
        strCity = txtFieldCity.text;
        
    }
    
    
    
    NSString * strWork =@"";
    
    if ( txtFieldWork.text != nil || ![txtFieldWork.text isEqual:@""])
        
    {
        
        //do your work
        
        
        
        strWork = @"";
        
    }else{
        
        strWork = txtFieldWork.text;
        
    }
    
    NSString * strCountry =@"";
    
    if (txtFieldCountry.text.length > 0 || txtFieldCountry.text != nil || ![txtFieldCountry.text isEqual:@""])
        
    {
        
        //do your work
        
        
        
        strCountry = @"";
        
    }else{
        
        strCountry = txtFieldCountry.text;
        
    }
    
    
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],@"DOB":txtField_DOB.text,@"UserName":txtFieldTitle.text,@"AboutMe":strAboutMe,@"City":strCity,@"Country":strCountry,@"Work":strWork,@"Distence":[NSString stringWithFormat:@"%f",slider.value],@"Gender":strGender,@"Intrest":strInterest,@"MobileNo":strPic};
    
    
    NSLog(@"TP1 : %@",urlParam);
    
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        
        NSLog(@"TP2 : %@",receivedData);
        
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            
            NSMutableDictionary *dictMutable = [[receivedData valueForKey:@"data"] mutableCopy];
            
            [dictMutable enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                
                //settting the null values to empty string
                if ([dictMutable valueForKey:key] == [NSNull null]) {
                    [dictMutable setValue:@"" forKey:key ];
                }
            }];

            [[NSUserDefaults standardUserDefaults] setValue:dictMutable forKey:@"userInfo"];
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            HomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
    }];
}


#pragma mark - UITextField delegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
    
}




- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == txtField_DOB && [txtField_DOB.text isEqualToString:@""]) {
        
        NSLog(@"%@",[datePicker date]);
        
        txtField_DOB.text = [dateConversion convertStringFromDate:[datePicker date] time:NO];
        
    }
    
    return TRUE;
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    
    if (txtFieldAboutMe == textView) {
        
        if (txtFieldAboutMe.text.length < 120) {
            
            return text;
            
        }else{
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Only 120 Characters are allowed." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        
        
        
    }
    
    
    
    
    
    return text;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // Prevent crashing undo bug – see note below.
    
    
    
    if (textField == txtFieldTitle) {
        
        
        
        if(range.length + range.location > txtFieldTitle.text.length)
            
        {
            
            return NO;
            
        }
        
        
        
        NSUInteger newLength = [txtFieldTitle.text length] + [string length] - range.length;
        
        return newLength <= 25;
        
    }
    
    return YES;
    
}

- (IBAction)btnTappedMenu:(id)sender {
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [self.revealViewController revealToggle:sender];
    
}


-(void)SetProfileImage

{
    
    
    
    UIActionSheet * actionSheetForImage = nil;
    
    
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        
    {
        
        actionSheetForImage = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Use Gallery", nil];
        
        [actionSheetForImage setTag:1];
        
    }
    
    else
        
    {
        
        actionSheetForImage = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Gallery", nil];
        
        [actionSheetForImage setTag:2];
        
    }
    
    
    
    [actionSheetForImage showInView:self.view];
    
    
    
    
    
}


#pragma mark -

-(void)openPictureViewWithCamera:(BOOL)camera

{
    
    UIImagePickerController * imagePicker =[[UIImagePickerController alloc]init];
    
    [imagePicker setDelegate:self];
    
    
    
    if (camera)
        
    {
        
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }
    
    else
        
    {
        
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }
    
    
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    
    
    if (![[UIApplication sharedApplication] isStatusBarHidden])
        
    {
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
    }
    
    UIImage *imagePickedFromLib;
    
    NSURL *mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    
    if (mediaUrl == nil){
        
        imagePickedFromLib = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        
        if (imagePickedFromLib == nil){
            
            
            
            imagePickedFromLib = (UIImage *)[info valueForKey:UIImagePickerControllerOriginalImage];
            
            if (isImageFirst == true) {
                
                [imgUser1 setImage:imagePickedFromLib];
                
            }else if (isImageSecond == true) {
                
                [imgUser2 setImage:imagePickedFromLib];
                
            }else if (isImageThird == true) {
                
                [imgUser3 setImage:imagePickedFromLib];
                
            }else if (isImageFour == true) {
                
                [imgUser4 setImage:imagePickedFromLib];
                
            }else if (isImageFive == true) {
                
                [imgUser5 setImage:imagePickedFromLib];
                
            }
            
            
            
        }
        
        else{
            
            if (isImageFirst == true) {
                
                [imgUser1 setImage:imagePickedFromLib];
                
            }else if (isImageSecond == true) {
                
                [imgUser2 setImage:imagePickedFromLib];
                
            }else if (isImageThird == true) {
                
                [imgUser3 setImage:imagePickedFromLib];
                
            }else if (isImageFour == true) {
                
                [imgUser4 setImage:imagePickedFromLib];
                
            }else if (isImageFive == true) {
                
                [imgUser5 setImage:imagePickedFromLib];
                
            }
            
            
            
        }
        
        
        
        //        [self updateProfileWebservice];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self saveImage:imagePickedFromLib];
    
}



#pragma mark - UIActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex

{
    
    if (actionSheet.tag == 1)//Camera
        
    {
        
        if (buttonIndex == 0)
            
        {
            
            [self openPictureViewWithCamera:YES];
            
        }
        
        else if(buttonIndex == 1)
            
        {
            
            [self openPictureViewWithCamera:NO];
            
        }
        
    }
    
    else if (actionSheet.tag == 2)//without camera
        
    {
        
        if (buttonIndex == 0)
            
        {
            
            [self openPictureViewWithCamera:NO];
            
        }
        
        
        
    }
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker

{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
    
}


-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}



-(void)saveImage:(UIImage *)image{
    
    
    
    //    NSString * strBase64 = [self encodeToBase64String:image];
    
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.4);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData1 = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
//    
//    NSString *strBase64 = [self imageToNSString:image];
//    NSString *strBase641 = [self imageToNSString:image scale:0.25 orientation:original.imageOrientation];
    
    NSString *strBase64 = [self imageToNSString:img];
    
    NSString * strID = @"0";
    
    NSString * strID1 = @"0";
    
    NSString * strPicID = @"0";
    
    if (isImageFirst == true) {
        
        if (arrProfilePic.count >=1) {
            
            strID = arrProfilePic[0];
            strID1 = @"1";
            
        }
        
        strPicID = @"1";
        
    }else if (isImageSecond == true) {
        
        if (arrProfilePic.count >=2) {
            
            strID = arrProfilePic[1];
            strID1 = @"2";
        }
        
    }else if (isImageThird == true) {
        
        if (arrProfilePic.count >=3) {
            
            strID = arrProfilePic[2];
            strID1 = @"3";
            
        }
        
    }else if (isImageFour == true) {
        
        if (arrProfilePic.count >=4) {
            
            strID = arrProfilePic[3];
            strID1 = @"4";
        }
        
    }else if (isImageFive == true) {
        
        if (arrProfilePic.count >=5) {
            
            strID = arrProfilePic[4];
            strID1 = @"5";
        }
        
    }
    
    
    
    
    
    NSString *url=[NSString stringWithFormat:SaveImageURL];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],@"AboutMe":strBase64,@"ImageID":strID,@"ProfilePic":strPicID,@"ImageID1":strID1 };
    
    
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        
        NSLog(@"TP2 : %@",receivedData);
        
        
        
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 2) {
            
            
            
            [arrProfilePic addObject:[receivedData valueForKey:@"ProfilePicID"]];
            
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
    }];
    
}


-(NSString *)imageToNSString:(UIImage *)image

{
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    return [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];;
    
}


- (NSString *)encodeToBase64String:(UIImage *)image {
    
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
}

@end

