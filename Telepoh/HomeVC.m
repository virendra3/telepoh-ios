//
//  HomeVC.m
//  Telepoh
//
//  Created by apple on 05/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "HomeVC.h"
#import "ChangePassword.h"
#import "CollectionViewCell.h"
#import "UserProfileVC.h"
#import "GraphView.h"
#import "NotificationVC.h"


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
@interface HomeVC ()
{
    NSMutableArray * arrEventList;
    int counterTime;
    Indicator * indicator;
    WebServiceConnection * newConnection;
    Validation * validateObj;
    NSString * strLat;
    NSString * strLong;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    validateObj = [Validation validationManager];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(UpdateNotification)
                                                 name:@"UpdateNotification"
                                               object:nil];
    
    arrEventList = [[NSMutableArray alloc] init] ;
    //strLat = @"26.911060";
    //strLong = @"75.737356";
    
    
    
    [collectionView.collectionViewLayout invalidateLayout];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    lblUserName.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"UserName"];
    
    
    AsyncImageView *imgView = (AsyncImageView *)imgUser;
    
    NSString * strURLImg = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ProfilePicDetails"][0] valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(loader)
                                   userInfo:nil
                                    repeats:YES];
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.distanceFilter = 1000;
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [locationManager startUpdatingLocation];
//     [self eventDataSearchWebservice];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived:)
                                                 name:@"pushNotificationAction"
                                               object:nil];
}

- (void) pushNotificationReceived:(NSNotification *) notification {
    
    UINavigationController *navigationController = (UINavigationController *)self.revealViewController.frontViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NotificationVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
    [navigationController pushViewController:dashboardViewController animated:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [locationManager startUpdatingLocation];
//    [self eventDataSearchWebservice];
}
- (void)viewDidAppear:(BOOL)animate{
    [super viewDidAppear:animate];
    //check if came through notification
    
    
    
}
-(void)viewWillLayoutSubviews{
    imgUser.layer.cornerRadius = imgUser.frame.size.height/2;
    imgUser.clipsToBounds = YES;
}
//- (void) UpdateNotification:(NSNotification *) notification
- (void) UpdateNotification {
    [self eventDataSearchWebservice];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Message" message:@"Failed to Get Your Current Location, Please enable your current Location from Setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        if (strLat.length == 0) {
            strLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            strLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            [self eventDataSearchWebservice];

        }
        
    }
}


#pragma mark - WebService Login method
-(void)eventDataSearchWebservice
{
    
    NSString *url=[NSString stringWithFormat:eventDataURL];
    int lastId =0;
    
    
    int distance = 0.5;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"]) {
        distance = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistance"] intValue];
    }
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],Lattitude_Key:strLat,Longitude_Key:strLong,UserDistence_Key:[NSNumber numberWithInt:distance],LastID_key:[NSNumber numberWithInt:lastId]};
    
//    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"My_Received %@",receivedData);
//        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrEventList = [[receivedData valueForKey:@"data"] mutableCopy] ;

            if (arrEventList.count != 0) {
                
                [UIView animateWithDuration:1.0 animations:^{
                    vwCollection.frame = CGRectMake(vwCollection.frame.origin.x, 75, vwCollection.frame.size.width, vwCollection.frame.size.height);
                }];
            }
            //            [arrEventList removeObjectAtIndex:1];
            
//            [[arrEventList mutableCopy] removeObjectAtIndex:1];
            [collectionView reloadData];
        }else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
        
    }];
    
}


-(void)loader{
  
    
    
    if (counterTime == 1) {
        imgRight1.image = [UIImage imageNamed:@"load-right1-act"];
        imgLeft1.image = [UIImage imageNamed:@"load-left1-act"];
    }else if (counterTime == 2) {
        imgRight2.image = [UIImage imageNamed:@"load-right2-act"];
        imgLeft2.image = [UIImage imageNamed:@"load-left2-act"];
    }
    
    else if (counterTime ==3) {
        imgRight2.image = [UIImage imageNamed:@"load-right2"];
        imgLeft2.image = [UIImage imageNamed:@"load-left2"];
    }else if (counterTime == 4) {
        imgRight1.image = [UIImage imageNamed:@"load-right1"];
        imgLeft1.image = [UIImage imageNamed:@"load-left1"];
    }
    if (counterTime == 4) {
        counterTime = 0;
    }
    counterTime = counterTime+1;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.

    [self performSegueWithIdentifier:@"ChangePassword" sender:self];
    
}


#pragma mark -
#pragma mark UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return arrEventList.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *myCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell"
                                    forIndexPath:indexPath];
    
    myCell.imgUser.layer.cornerRadius =myCell.imgUser.frame.size.height/2;
    myCell.imgUser.clipsToBounds = YES;
    
    myCell.btnIgnore.layer.cornerRadius = 10.0;
    myCell.btnIgnore.layer.borderColor =[UIColor grayColor].CGColor;
    myCell.btnIgnore.layer.borderWidth = 1.0;
    myCell.btnIgnore.clipsToBounds = YES;
    
    myCell.vwBackGround.layer.cornerRadius = 10.0;
    myCell.vwBackGround.layer.borderColor =[UIColor colorWithRed:255.0/255.0 green:108.0/255.0 blue:102.0/255.0 alpha:1.0].CGColor;
    myCell.vwBackGround.layer.borderWidth = 1.0;
    myCell.vwBackGround.clipsToBounds = YES;
    
    myCell.btnJoin.layer.cornerRadius = 10.0;
//    myCell.btnJoin.layer.borderWidth = 2.0;
    myCell.btnJoin.clipsToBounds = YES;
    
    myCell.lblUserName.text = [arrEventList[indexPath.row] valueForKey:@"UserName"];
    myCell.txtViewDescription.text = [arrEventList[indexPath.row] valueForKey:@"Title"];
    
    double distance = [[arrEventList[indexPath.row] valueForKey:@"CurrentDistence"] integerValue]/1000;
    
    NSString * strMile = @"";
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"]) {
        strMile = [[NSUserDefaults standardUserDefaults] valueForKey:@"SearchDistanceType"];
    }else{
        strMile = @"Mile";
    }
    
    myCell.lblCurrentLocation.text = [NSString stringWithFormat:@"Current Location     : %.1f %@",distance,strMile];
    if([[arrEventList[indexPath.row] valueForKey:@"MaxAllowedPeople"] integerValue]==0)
    {
        myCell.lblMacimumJoiners.text = [NSString stringWithFormat:@"Maximum Joiners   : No Limit"];
        
    }
    else
    {
        myCell.lblMacimumJoiners.text = [NSString stringWithFormat:@"Maximum Joiners   : %@",[arrEventList[indexPath.row] valueForKey:@"MaxAllowedPeople"]];
        
    }
    
    
   
    
//    "EventID": 53,
//    "UserDetailsID": 7,
//    "Title": "vejj1",
//    "UserName": "vijendra singh",
//    "UserProfilePic": null,
//    "UserRatings": 0,
//    "TimeLimit": "2016-04-21T15:47:13.257",
//    "CurrentDistence": 6318,
//    "MaxAllowedPeople": 0
    
    
    
    AsyncImageView *imgView = (AsyncImageView *)myCell.imgUser;
    
    
    NSString * strURLImg = [[arrEventList[indexPath.row] valueForKey:@"UserProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    
    myCell.starRating.starImage =[UIImage imageNamed:@"star"];
    myCell.starRating.starHighlightedImage = [UIImage imageNamed:@"starActive"];
    myCell.starRating.maxRating = 5.0;
    myCell.starRating.editable=NO;
    myCell.starRating.rating= [[arrEventList[indexPath.row] valueForKey:@"UserRatings"] floatValue];
    myCell.starRating.displayMode=EDStarRatingDisplayHalf;
    
//    UITapGestureRecognizer *userProfile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
//    [myCell.imgUser addGestureRecognizer:userProfile];
   

    GraphView *graphView = [[GraphView alloc] initWithFrame:CGRectMake(0, 0, myCell.vwTimerLoader.frame.size.width, myCell.vwTimerLoader.frame.size.height)];
    
    graphView.backgroundColor = [UIColor whiteColor];
    graphView.layer.borderColor = [UIColor redColor].CGColor;
    //    graphView.layer.borderWidth = 1.0f;
    [graphView loaderFrame:CGRectMake(60, 0, 40, 40) withLowerLayer:CGRectMake(63.5, 4, 33, 33)];
    
    [graphView loaderFrame:CGRectMake(110, 0, 40, 40) withLowerLayer:CGRectMake(113.5, 4, 33, 33)];
    
    [graphView loaderFrame:CGRectMake(160, 0, 40, 40) withLowerLayer:CGRectMake(163.5, 4, 33, 33)];
    // create dateFormatter with UTC time format
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:[arrEventList[indexPath.row] valueForKey:@"TimeLimit"]]; // create date from string
    
    NSDate *now = [NSDate date];
    
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitHour
                                               fromDate:date toDate:now options:0];
    
    NSDateComponents *differenceMins = [calendar components:NSCalendarUnitMinute
                                                   fromDate:date toDate:now options:0];
    
    
    NSDateComponents *differenceSeconds = [calendar components:NSCalendarUnitSecond
                                                      fromDate:date toDate:now options:0];
    
    
    NSInteger hour = ABS(difference.hour);
    NSInteger min;
    if (hour == 0) {
        min = ABS(differenceMins.minute);
    }else{
        min = ABS(differenceMins.minute%60);
    }
    
    
    NSInteger second;
    if (min == 0) {
        second = ABS(differenceSeconds.second);
    }else{
        second = ABS(differenceSeconds.second%min);
    }

    
    NSLog(@"hoursBetweenDates: %ld Min:%ld Seconds:%ld", (long)hour,(long)min,(long)second);
    
    NSDate *now1 = [NSDate date];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"hh:mm:ss";
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    NSLog(@"The Current Time is %@",[dateFormatter1 stringFromDate:now1]);
    
    
    [graphView timeManage:hour withMints:min withSeconds:second];
    
    //    [graphView startAnimating];
    [myCell.vwTimerLoader addSubview:graphView];
    myCell.btnJoin.tag = indexPath.row;
    myCell.btnIgnore.tag = indexPath.row;
    [myCell.btnJoin addTarget:self action:@selector(joinEvent:) forControlEvents:UIControlEventTouchUpInside];
    [myCell.btnIgnore addTarget:self action:@selector(IgnoreEvent:) forControlEvents:UIControlEventTouchUpInside];
    myCell.btnProfile.tag = indexPath.row;
    [myCell.btnProfile addTarget:self action:@selector(userProfileTapped:) forControlEvents:UIControlEventTouchUpInside];
    return myCell;
}


-(void)userProfileTapped:(UIButton *)sender{
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    dashboardViewController.dictInfoUser = arrEventList[sender.tag];
    [self.navigationController pushViewController:dashboardViewController animated:NO];
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    UIStoryboard *storyboard;
//    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
//    dashboardViewController.dictInfoUser = arrEventList[indexPath.row];
//    [self.navigationController pushViewController:dashboardViewController animated:NO];
}
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  
    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
}
//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    int numberOfCellInRow = 1;
//    CGFloat cellWidth =  [[UIScreen mainScreen] bounds].size.width/numberOfCellInRow;
//    return CGSizeMake(cellWidth, cellWidth);
//}


-(void)joinEvent:(UIButton *)sender{
    UIButton * btn = sender;
    
    NSLog(@"%ld",(long)btn.tag);
    
    NSString *url=[NSString stringWithFormat:AcceptEventURL];
    [self eventActivityAcceptAndIgnore:url withEventID:[[arrEventList[btn.tag] valueForKey:@"EventID"] intValue] withIndex:(int)btn.tag withType:@"iOS"];
   
}

-(void)IgnoreEvent:(UIButton *)sender{
    UIButton * btn = sender;
    
    NSLog(@"%ld",(long)btn.tag);
//    NSString *url=[NSString stringWithFormat:IgnoreEventURL];
//    [self eventActivityAcceptAndIgnore:url withEventID:[[arrEventList[btn.tag] valueForKey:@"EventID"] intValue] withIndex:(int)btn.tag withType:@""];
    [arrEventList  removeObjectAtIndex:(int)btn.tag];
    if (arrEventList.count == 0) {
        [UIView animateWithDuration:1.0 animations:^{
            vwCollection.frame = CGRectMake(vwCollection.frame.origin.x, 800, self.view.frame.size.width, vwCollection.frame.size.height);
        }];
        AsyncImageView *imgView = (AsyncImageView *)imgUser;
        
        NSString * strURLImg = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ProfilePicDetails"][0] valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    }

    
    [collectionView reloadData];
    
}

-(void)eventActivityAcceptAndIgnore:(NSString *)url withEventID:(int )eventID withIndex:(int)index withType:(NSString *)strType{
    
    NSDictionary *urlParam;
    if ([strType isEqualToString:@"iOS"]) {
        urlParam=@{LastID_key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],ID_Key:[NSNumber numberWithInt:eventID],@"Type":@"iOS"};
    }else{
        urlParam=@{LastID_key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],ID_Key:[NSNumber numberWithInt:eventID]};
    }
    
    
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
//        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
//            NSInteger count = 0;
//            for (NSDictionary * dict in arrEventList) {
//                if ([[dict valueForKey:@"EventID"] intValue]  == eventID) {
//                    [arrEventList  removeObjectAtIndex:count];
//                    count =  count+1;
//                }
//            }
            [arrEventList  removeObjectAtIndex:index];
            if (arrEventList.count == 0) {
                [UIView animateWithDuration:1.0 animations:^{
                    vwCollection.frame = CGRectMake(vwCollection.frame.origin.x, 800, self.view.frame.size.width, vwCollection.frame.size.height);
                }];
                AsyncImageView *imgView = (AsyncImageView *)imgUser;
                
                NSString * strURLImg = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ProfilePicDetails"][0] valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [imgView setImageURL:[NSURL URLWithString:strURLImg]];
            }
            [collectionView reloadData];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
//        }else{
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
        
    }];
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    [self.navigationController pushViewController:dashboardViewController animated:YES];
}
- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}

- (IBAction)btnTappedLocation:(id)sender {
}
@end
