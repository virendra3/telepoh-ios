//
//  HomeVC.h
//  Telepoh
//
//  Created by apple on 05/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import <MapKit/MapKit.h>

@interface HomeVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate>
{
    IBOutlet UICollectionView *collectionView;
    
    IBOutlet UILabel *lblUserName;
    IBOutlet UIImageView *imgUser;
    IBOutlet UIImageView *imgRight1;
    IBOutlet UIImageView *imgRight2;
    
    IBOutlet UIView *vwCollection;
   
    IBOutlet UIImageView *imgLeft1;
    IBOutlet UIImageView *imgLeft2;
    CLLocationManager * locationManager;
}


- (IBAction)btnTappedMenu:(id)sender;
- (IBAction)btnTappedLocation:(id)sender;

@end
