//
//  SideMenuVC.h
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuVC : UIViewController
{
    IBOutlet UIImageView *userImg;
    IBOutlet UILabel *lblUserName;
    IBOutlet UITableView *tblView;
    
}
- (IBAction)btnTappedProfile:(id)sender;

@end
