//
//  SideMenuVC.m
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "SideMenuVC.h"
#import "SideMenuCustomCell.h"
#import "ChangePassword.h"
#import "PostActivityVC.h"
#import "ViewController.h"
#import "HomeVC.h"
#import "InboxVC.h"
#import "EditProfileVC.h"
#import "NotificationVC.h"
#import "PostedActivityVC.h"
#import "JoinedActivityVC.h"
#import "UserProfileVC.h"

@interface SideMenuVC ()
{
    NSArray * arrMenuLbl;
    NSArray * arrImage;
    UITabBar * tabbar ;
    Indicator * indicator;
    WebServiceConnection * newConnection;
    
}

@end

@implementation SideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrMenuLbl = @[@"All Activity",@"Post New Activity",@"Edit Profile",@"Message",@"My Posted Activity",@"Joined Activity",@"Notification",@"Change Password",@"Share",@"Rate Telepoh",@"Logout"];
    arrImage = @[@"all-activities",@"post",@"myprofile",@"message",@"posted",@"joined",@"notification",@"profile",@"share",@"rate",@"signout"];
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    
    //userImg.layer.cornerRadius = 40.0;
    //userImg.clipsToBounds = YES;
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    
    userImg.layer.cornerRadius = 40.0;
    userImg.clipsToBounds = YES;
    
    lblUserName.text = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"UserName"];
    
    AsyncImageView *imgView = (AsyncImageView *)userImg;
    
    NSString * strURLImg = [[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ProfilePicDetails"][0] valueForKey:@"PicPath"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [arrMenuLbl count];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    SideMenuCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuCustomCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.lblActionName.text =arrMenuLbl[indexPath.row];
    cell.imgAction.image = [UIImage imageNamed:arrImage[indexPath.row]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UINavigationController *navigationController = (UINavigationController *)self.revealViewController.frontViewController;
    NSLog(@"%@",[navigationController viewControllers]);
    
    UIViewController *viewController = nil;
    UIStoryboard *storyboard;
    
    
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BOOL check = FALSE;
    if (indexPath.row == 0) {
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[HomeVC class]]) {
                check = TRUE;
                break;
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateNotification" object:self];
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            HomeVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    
    
    else if (indexPath.row == 1) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[PostActivityVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            PostActivityVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"PostActivityVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    else if (indexPath.row == 2) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[EditProfileVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            EditProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    else if (indexPath.row == 3) {
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[InboxVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            InboxVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"InboxVC"];
            dashboardViewController.strLastController = @"Menu";
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    else if (indexPath.row == 4) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[PostedActivityVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            PostedActivityVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"PostedActivityVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    else if (indexPath.row == 5) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[JoinedActivityVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            JoinedActivityVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"JoinedActivityVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    else if (indexPath.row == 6) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[NotificationVC class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            NotificationVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        [self.revealViewController revealToggleAnimated:YES];
    }
    
    else if (indexPath.row == 7) {
        
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[ChangePassword class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            ChangePassword *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        //if declare like this
        
        
        //        [[NSNotificationCenter defaultCenter]
        //         postNotificationName:@"TestNotification"
        //         object:self];
        
        [self.revealViewController revealToggleAnimated:YES];
    }else if (indexPath.row == 10){
        
        [[GIDSignIn sharedInstance] signOut];
        [self logoutWebservice];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfo"];
        
        for (viewController in [navigationController viewControllers]) {
            if ([viewController isKindOfClass:[ViewController class]]) {
                check = TRUE;
                break;
            }
        }
        if (check) {
            [navigationController popToViewController:viewController animated:NO];
        }
        else{
            ViewController *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            [navigationController pushViewController:dashboardViewController animated:NO];
            
        }
        
        
        [self.revealViewController revealToggleAnimated:YES];
        
    }
}


#pragma mark - WebService Log Out method
-(void)logoutWebservice
{
    
    NSString *url=[NSString stringWithFormat:LogoutURL];
    
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    //    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        //         [indicator removeFromSuperview];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"RefereshLoginView"
         object:self];
        
        
    }];
    
}

- (IBAction)btnTappedProfile:(id)sender {
    
    
    UINavigationController *navigationController = (UINavigationController *)self.revealViewController.frontViewController;
    NSLog(@"%@",[navigationController viewControllers]);
    
    UIStoryboard *storyboard;
    
    
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    dashboardViewController.dictInfoUser = [[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"];
    //    dashboardViewController.dictInfoUser = arrEventList[sender.tag];
    [navigationController pushViewController:dashboardViewController animated:NO];
    [self.revealViewController revealToggleAnimated:YES];
    
    
    
}
@end
