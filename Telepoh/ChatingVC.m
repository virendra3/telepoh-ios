//
//  ChatingVC.m
//  Telepoh
//
//  Created by apple on 21/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "ChatingVC.h"
#import "ChatMessageCell.h"
#import "DelinkVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface ChatingVC ()<UITextFieldDelegate>
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    NSMutableArray * arrMessageList;
    NSTimer * timer;
    BOOL isKeyBoardOpen;
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMessge;
@property (weak, nonatomic) IBOutlet UIView *vwMessageSend;

- (IBAction)btnTappedBack:(id)sender;
- (IBAction)btnSend:(id)sender;

@end

@implementation ChatingVC
@synthesize dictInfo;

#pragma mark - UIViewController methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpVC];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [self.vwMessageSend setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    isKeyBoardOpen = false;
    self.txtFieldMessge.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [super viewWillAppear:(BOOL)animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeDelinkView:)
                                                 name:@"removeDelinkView"
                                               object:nil];
    // To Handle Keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppear:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:(BOOL)animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //self.tableHeightConstraint.constant = self.view.frame.size.height - 125;
    
        [[IQKeyboardManager sharedManager] setEnable:YES];
//self.tableHeightConstraint.constant = self.view.frame.size.height - 125;
}

-(void)viewDidAppear:(BOOL)animated{
    self.tableHeightConstraint.constant = self.view.frame.size.height - 125;
    [self.vwMessageSend setHidden:NO];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    [self invalidateTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"Delink"]) {
        DelinkVC * vc = segue.destinationViewController;
        vc.dictInfo = self.dictInfo;
    }
}


#pragma mark - UITableView Datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMessageList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (arrMessageList.count <= 1) {
//        return 250;
//    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:13.0]};
    NSString *strMessage = [NSString stringWithFormat:@"%@",[arrMessageList[indexPath.row] valueForKey:@"Message"]];
    CGRect rect = [strMessage boundingRectWithSize:CGSizeMake(240, CGFLOAT_MAX)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:attributes
                                           context:nil];
    NSLog(@"%f", rect.size.height );
    return (rect.size.height + 50);
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifierMessageCellReceiver = @"ChatMessageCell";
    static NSString *cellIdentifierMessageCellSender = @"ChatMessageCellSender";
    
    ChatMessageCell* cell;
    NSDictionary *dicMessage = (NSDictionary*) arrMessageList[indexPath.row];
    
    if ([[dicMessage valueForKey:@"SenderID"] intValue] == [[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]) {
        cell = (ChatMessageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifierMessageCellSender];
    }else{
        
        cell = (ChatMessageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifierMessageCellReceiver];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.vcBackground.layer.borderWidth = 1.0;
    cell.vcBackground.layer.cornerRadius = 10.0;
    cell.vcBackground.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblUserName.text = [NSString stringWithFormat:@"%@",[dicMessage valueForKey:@"UserName"]];;
    cell.lblMessage.text = [NSString stringWithFormat:@"%@",[dicMessage valueForKey:@"Message"]];;
    cell.lblTime.text = [NSString stringWithFormat:@"%@",[dicMessage valueForKey:@"SendDate1"]];;
    return (UITableViewCell*)cell;
}

#pragma mark - UIButton action methods
- (IBAction)btnTappedBack:(id)sender {
    [self invalidateTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSend:(id)sender {
    [self sendMessageWebservice];
}


#pragma mark - UITextFieldDelegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""] || [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0){
        [textField setText:@""];
        textField.placeholder = @"Type your message";
    }
}


#pragma mark - supporting methods
- (void) setUpVC{
    
    CGFloat tblHeight = 0.0;
//    CGFloat y = [self navigationBarHt] + [self getStatusBarFrame];
    CGFloat y = [self navigationBarHt] + [self getStatusBarFrame] +self.vwMessageSend.frame.size.height;
    tblHeight = self.view.frame.size.height - self.vwMessageSend.frame.size.height - y;
    self.tblView.frame = CGRectMake(0,y,self.view.frame.size.width, tblHeight);
    y += tblHeight;
    CGRect framevwMessageSend = self.vwMessageSend.frame;
    framevwMessageSend.origin.x = 0;
    framevwMessageSend.origin.y = y;
    framevwMessageSend.size.width = self.view.frame.size.width;
    self.vwMessageSend.frame = framevwMessageSend;
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    arrMessageList = [[NSMutableArray alloc] init];
    newConnection=[WebServiceConnection connectinManager];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.tblView addGestureRecognizer:tapGesture];
    [self startTimer];
    [self showAllMessageWebservice];
}

-(void)hideKeyBoard {
    [self.txtFieldMessge resignFirstResponder];
}

- (void) scrollToLastMessage{
    if ([arrMessageList count]>0) {
            [self.tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:arrMessageList.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }

}

- (void) startTimer{
    timer= [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(showAllMessageWebservice) userInfo:nil repeats:YES];
}

- (void) invalidateTimer{
    [timer invalidate];
    timer = nil;
}

- (CGFloat) getStatusBarFrame{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

-(CGFloat) navigationBarHt {
    return self.navigationController.navigationBar.frame.size.height;
}

#pragma mark - webservices method
-(void)sendMessageWebservice{
    
    [self invalidateTimer];
    if (self.txtFieldMessge.text && self.txtFieldMessge.text.length > 0)
    {
        [self.view endEditing:YES];
        
        NSString *url=[NSString stringWithFormat:SendMessageURL];
        NSDictionary *urlParam=@{
                                 @"SenderID":[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],
                                 @"ID":[self.dictInfo valueForKey:@"ID"],
                                 @"Message":self.txtFieldMessge.text,
                                 @"ConversationsID":[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]
                                 };
        
        [self.view addSubview:indicator];
        [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
            NSLog(@"%@",receivedData);
            [indicator removeFromSuperview];
            [self startTimer];
            
            if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
                //            arrMessageList = [[receivedData valueForKey:@"data"] mutableCopy];
                //            [self.tblView reloadData];
                self.txtFieldMessge.text = @"";
                if (arrMessageList.count >= 1) {
                    //                    NSIndexPath* ipath = [NSIndexPath indexPathForRow: arrMessageList.count-1  inSection: 0];
                    //                    [self.tblView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
                }
                [self showAllMessageWebservice];
            }else{
                if ([[receivedData valueForKey:@"Message"] isEqualToString:@"User is Delinked from Other End"]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }];
    }
//    else
//    {
//        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Type Something" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert1 show];
//    }
    
}

-(void)showAllMessageWebservice{
    
    NSString *url=[NSString stringWithFormat:ShowAllMessageURL];
    NSDictionary *urlParam=@{
                             @"SenderID":[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],
                             @"ID":[self.dictInfo valueForKey:@"ID"]
                             };
    
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"receivedData : %@",receivedData);
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            
            [arrMessageList removeAllObjects];
            arrMessageList = [[receivedData valueForKey:@"data"] mutableCopy];
            [self.tblView reloadData];
            [self scrollToLastMessage];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP1" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}


#pragma mark - Keyboard appearance/disappearance handling
- (void)keyboardWillAppear:(NSNotification *)notification{
    if (isKeyBoardOpen == false) {
        isKeyBoardOpen = true;
    
        NSDictionary *userInfo = [notification userInfo];
        CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue].size;
        
        self.tableHeightConstraint.constant = self.tableHeightConstraint.constant - keyboardSize.height;

        [self.tblView layoutIfNeeded];
    
 /*   UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    [self.tblView setContentInset:contentInsets];
    [self.tblView setScrollIndicatorInsets:contentInsets];
        
      */
   // [self scrollToLastMessage];
    
    CGRect messageFrame = self.vwMessageSend.frame;
    messageFrame.origin.y -= keyboardSize.height;
    //[self.vwMessageSend setFrame:messageFrame];
        
    [self.tblView reloadData];
        
    [self scrollToLastMessage];


    }
}

- (void)keyboardWillDisappear:(NSNotification *)notification{
    isKeyBoardOpen = false;

    NSDictionary *userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue].size;
    
     self.tableHeightConstraint.constant = self.tableHeightConstraint.constant + keyboardSize.height;
    
    [self.tblView layoutIfNeeded];
    
    
  /*  [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [self.tblView setContentInset:UIEdgeInsetsZero];
    [UIView commitAnimations];
    [self.tblView setScrollIndicatorInsets:UIEdgeInsetsZero];
    */
   // CGRect messageFrame = self.vwMessageSend.frame;
   // messageFrame.origin.y += keyboardSize.height;
  //  [self.vwMessageSend setFrame:messageFrame];
    [self.tblView reloadData];
    
    [self scrollToLastMessage];


}

#pragma mark - DelinkView handling

- (void) removeDelinkView:(NSNotification *) notification{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
