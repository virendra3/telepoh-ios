//
//  UserImageVC.h
//  Telepoh
//
//  Created by apple on 21/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserImageVC : UIViewController<UIScrollViewDelegate>{
    
    IBOutlet UIButton *btnAbout;
    IBOutlet UIImageView *profileImage;
    IBOutlet UILabel *UserPost;
    IBOutlet UILabel *UserAge;
    IBOutlet UILabel *lblUserName;
}
@property(nonatomic ,retain)NSDictionary * dictInfo;
- (IBAction)btnTappedBack:(id)sender;

@end
