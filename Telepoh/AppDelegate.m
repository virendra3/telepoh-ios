//
//  AppDelegate.m
//  Telepoh
//
//  Created by Admin on 4/4/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import "AppDelegate.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
//#import <FacebookSDK/FacebookSDK.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>


#import "NotificationVC.h"
@import Firebase;
@import UserNotifications;

#define GOOGLE_SCHEME @"com.abc.Telepoh"

@interface AppDelegate ()

@end


// HI, Please type ur system password and allow it.
// Same procedure has to for Key.
// Now certs & key are exported.
// Password is 12345
// Mail all this .p12 to me @shivajinpawar@gmail.com

@implementation AppDelegate 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    sleep(3.0);
    
    [[IQKeyboardManager sharedManager]setEnable:true];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    
    [FIRApp configure];
    // [FIRMessaging messaging].remoteMessageDelegate = self;
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    if (launchOptions != NULL) {
        //Has come through push notification
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"]) {
        //means user if logged in
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                // Delay execution by 5 seconds.
                [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotificationAction" object:launchOptions];
            });
        }
    }
    
    return YES;
}
/*
 - (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
 NSLog(@"\n\n\n FCM registration token: %@\n\n\n\n", fcmToken);
 // Notify about received token.
 NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
 [[NSNotificationCenter defaultCenter] postNotificationName:
 @"FCMToken" object:nil userInfo:dataDict];
 
 // TODO: If necessary send token to application server.
 // Note: This callback is fired at each app startup and whenever a new token is generated.
 }
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSString *deviceToken = [[devToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"\n\n DeviceToken: content---%@\n\n", deviceToken);
    //UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:deviceToken delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    //[alert show];
    
    NSString *fcmToken = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults]setObject:fcmToken forKey:@"udidKey"];
    
    NSLog(@"\n\n fcmToken:  %@\n\n",fcmToken );
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
    
}
//#endif
//


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSDictionary *apsInfo=[userInfo objectForKey:@"aps"];
    NSLog(@"apsInfo==>>%@",apsInfo);
    //check if user if logged in
    
    /*
     NSInteger  badgeCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
     badgeCount = badgeCount + 1;
     [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
     // NSString *alert=[apsInfo objectForKey:@"alert"];
     
     UILocalNotification *localNotif = [[UILocalNotification alloc] init];
     // localNotif.fireDate = date;  // date after 10 sec from now
     localNotif.timeZone = [NSTimeZone defaultTimeZone];
     
     // Notification details
     localNotif.alertBody =  [[apsInfo objectForKey:@"link"] objectForKey:@"message"]; // text of you that you have fetched
     // Set the action button
     localNotif.alertAction = @"View";
     
     localNotif.soundName = UILocalNotificationDefaultSoundName;
     // localNotif.applicationIconBadgeNumber = 1;
     
     // Specify custom data for the notification
     NSDictionary *infoDict = [NSDictionary dictionaryWithObject:[[apsInfo objectForKey:@"link"] objectForKey:@"comment"] forKey:@"commentKey"];
     localNotif.userInfo = infoDict;
     
     // Schedule the notification
     //[[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
     
     [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
     //completionHandler(UIBackgroundFetchResultNewData);
     */
}

- (void)userNotificationCenter:(UNUserNotificationCenter* )center willPresentNotification:(UNNotification* )notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSLog( @"Here handle push notification in foreground" );
    completionHandler(UNNotificationPresentationOptionAlert);
    // Print Notification info
   // NSLog(@"Userinfo %@",notification.request.content.userInfo);
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSDictionary *apsInfo=[userInfo objectForKey:@"aps"];
    NSLog(@"apsInfo: from completionHandler ==>>%@",apsInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotificationAction" object:userInfo];
    
    /*  UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     NotificationVC *ivc = [mainstoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
     //    ivc.dictGetMessage =apsInfo;
     UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:ivc];
     self.window.rootViewController =nil;
     self.window.rootViewController = navigationController;
     [self.window makeKeyAndVisible];
     // NSString *alert=[apsInfo objectForKey:@"alert"];
     
     //    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
     */
    
}
/*
 - (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
 {
 // If the session was opened successfully
 
 if (!error && state == FBSessionStateOpen){
 NSLog(@"Session opened");
 // Show the user the logged-in UI
 //        [self userLoggedIn];
 return;
 }
 if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
 // If the session is closed
 NSLog(@"Session closed");
 // Show the user the logged-out UI
 //        [self userLoggedOut];
 }
 
 // Handle errors
 if (error){
 NSLog(@"Error");
 NSString *alertText;
 NSString *alertTitle;
 // If the error requires people using an app to make an action outside of the app in order to recover
 if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
 alertTitle = @"Something went wrong";
 alertText = [FBErrorUtility userMessageForError:error];
 //            [self showMessage:alertText withTitle:alertTitle];
 } else {
 
 // If the user cancelled login, do nothing
 if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
 NSLog(@"User cancelled login");
 
 // Handle session closures that happen outside of the app
 } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
 alertTitle = @"Session Error";
 alertText = @"Your current session is no longer valid. Please log in again.";
 //                [self showMessage:alertText withTitle:alertTitle];
 
 // For simplicity, here we just show a generic message for all other errors
 // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
 } else {
 //Get more error information from the error
 NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
 
 // Show the user an error message
 alertTitle = @"Something went wrong";
 alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
 //                [self showMessage:alertText withTitle:alertTitle];
 }
 }
 // Clear this token
 [FBSession.activeSession closeAndClearTokenInformation];
 // Show the user the logged-out UI
 //        [self userLoggedOut];
 }
 }
 */

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    
//        if ([[url scheme] isEqualToString:GOOGLE_SCHEME])
//            return [GPPURLHandler handleURL:url
//                          sourceApplication:sourceApplication
//                                 annotation:annotation];
//    
////    return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
//
//        
//    
//    return [FBSession.activeSession handleOpenURL:url];
//}

//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options {
//    
//    if([url.scheme isEqualToString:@"fb1671917423071936"]){
//        return [FBSession.activeSession handleOpenURL:url];
//    }
//    else{
//        return [GPPURLHandler handleURL:url
//                //                          sourceApplication:sourceApplication
//                //                                 annotation:annotation];
//    }
//}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    
//    if([url.scheme isEqualToString:@"fb1671917423071936"]){
//        return [FBSession.activeSession handleOpenURL:url];
//    }
//    else{
//       // return [GPPURLHandler handleURL:url
//                                          sourceApplication:sourceApplication
//                                                 annotation:annotation];
//    }
//}
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    
    if([url.scheme isEqualToString:@"fb1671917423071936"]){
        // return [FBSession.activeSession handleOpenURL:url];
        return  true;
    }
    else{
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    if([url.scheme isEqualToString:@"fb1671917423071936"]){
        // return [FBSession.activeSession handleOpenURL:url];
        
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url  sourceApplication:sourceApplication  annotation:annotation];
        return handled;
        
        // return  true;
    }
    else{
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
}


@end
