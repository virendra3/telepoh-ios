//
//  ViewController.m
//  Telepoh
//
//  Created by Admin on 4/4/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import "ViewController.h"
//#import <FacebookSDK/FacebookSDK.h>
#define thumbSize CGSizeMake(130, 150)

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import Firebase;


@interface ViewController ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    Validation * validateObj;
    int userIDLogout;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    validateObj = [Validation validationManager];
    btnLogin.layer.cornerRadius = 10.0f;
    btnLogin.clipsToBounds = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refereshLoginView:)
                                                 name:@"RefereshLoginView"
                                               object:nil];
   
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"]) {
        [self loginCheckFunction];
    }
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].clientID = @"644181336452-p93robju4u201d7b8q3rmkb3gd5i77ql.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/plus.me",@"profile",@"http://www.google.com/m8/feeds/"];
    
}

- (void) refereshLoginView:(NSNotification *) notification
{
    [self viewDidLoad];
}

-(void)loginCheckFunction{
    NSString *url=[NSString stringWithFormat:CheckLoginURL];
    
    NSString * strUDID =  [[FIRInstanceID instanceID] token];
    if (strUDID == nil) {
        strUDID = @"1234568970";
    }
    
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]],@"DeviceID":strUDID};
    
        [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        [indicator removeFromSuperview];
        if ([[receivedData valueForKey:@"ID"]intValue] == 0) {
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfo"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@" User is Logged in From Other Device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
           
        }else{
            [self performSegueWithIdentifier:@"loginToHome" sender:self];
        }
        
        
    }];
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"Prepare for segue %@",segue.identifier);
}



- (IBAction)btnTappedLogin:(id)sender {
    NSString *message = nil;
    
    if (![validateObj validateBlankField:[txtFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Email";
    }
    else if (![validateObj validateEmail:[txtFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Proper Email";
    }
    else if (![validateObj validateBlankField:[txtFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Password";
    }
    
    if ([message length]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else{
        [self loginWebservice];
    }
}

- (IBAction)btnTappedGoogleLogin:(id)sender {
    
//    signIn = [GPPSignIn sharedInstance];
//    signIn.shouldFetchGooglePlusUser = YES;
////    signIn = [GPPSignIn sharedInstance];
//    signIn.clientID =@"644181336452-p93robju4u201d7b8q3rmkb3gd5i77ql.apps.googleusercontent.com";
//    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
//    
//    signIn.shouldFetchGoogleUserEmail = YES;
//    signIn.delegate = self;
//    signIn.scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/plus.me",@"profile",@"http://www.google.com/m8/feeds/"];
//    
//    
//    //   / signIn.scopes= @[];
//    //signIn.scopes= @[@"https://www.googleapis.com/auth/plus.login",@"http://www.google.com/m8/feeds/"];
//    
//    [signIn authenticate];
    
    
      [[GIDSignIn sharedInstance] signIn];
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
}



- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error{
    if (error != nil){
       NSLog(@"------ %@",error.localizedDescription);
        return;
    }
    
    
    NSLog(@"Email %@",user.profile.email);
    NSLog(@"Name %@",user.profile.name);
    NSString *imageURL = @"";
    if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
    {
        NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
        imageURL = [NSString stringWithFormat:@"%@",[user.profile imageURLWithDimension:dimension]];
    }
    [self loginWithGmail:user.profile.name withEmail:user.profile.email withImageURL:imageURL];
    
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error{
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error{
    
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    [self presentViewController:viewController animated:YES completion:nil];
}

// If implemented, this method will be invoked when sign in needs to dismiss a view controller.
// Typically, this should be implemented by calling |dismissViewController| on the passed
// view controller.
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)btnTappedForget:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Forgot Password ?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *place=[alert textFieldAtIndex:0];
    place.placeholder=@"Enter Email Address";
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 101) {
        if (buttonIndex == 1) {
            NSString *url=[NSString stringWithFormat:LogoutURL];
            
            
            NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:userIDLogout]};
            
//                [self.view addSubview:indicator];
            [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
                NSLog(@"%@",receivedData);
//                [indicator removeFromSuperview];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [self viewDidLoad];
                
            }];
        }
    }else{
        if (buttonIndex == 1) {
            NSString *name = [alertView textFieldAtIndex:0].text;
            // Insert whatever needs to be done with "name"
            NSLog(@"%@",name);
            [self ForgetWebservice:name];
        }
    }
    
    
}

#pragma mark - WebService Login method
-(void)ForgetWebservice:(NSString *)email
{
    NSString *url=[NSString stringWithFormat:forgetURL];
    NSDictionary *urlParam=@{@"Email":email};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}
//GPPSignIn *signIn = [GPPSignIn sharedInstance];
//signIn = [GPPSignIn sharedInstance];
//signIn.delegate = self;
//signIn.shouldFetchGoogleUserEmail = YES;
//signIn.clientID= @"81183350673-e46ulo8n9hsohphshsk3ihfhk7qonhiv.apps.googleusercontent.com";
//signIn.scopes = [NSArray arrayWithObjects:kGTLAuthScopePlusLogin,nil];
//signIn.actions = [NSArray arrayWithObjects:@"http://schemas.google.com/ListenActivity",nil];
//[signIn authenticate];

- (void)didDisconnectWithError:(NSError *)error{
    NSLog(@"%@",error);
}
//    - (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
//error: (NSError *) error {
//    
//    if (error) {
//        // [appdelegate.spinner stopAnimating];
//        //appdelegate.spinner removeFromSuperview];
//        NSLog(@"Received error %@ and auth object %@",error, auth);
//        return;
//    }
//    //lbl_title1.frame=CGRectMake(0, ([[UIScreen mainScreen] bounds].size.height-40)/2-40, [[UIScreen mainScreen] bounds].size.width, 40);
//    //activity.frame=CGRectMake(([[UIScreen mainScreen] bounds].size.width-20)/2, lbl_title1.frame.origin.y+50, 20,  20);
//    // The googlePlusUser member will be populated only if the appropriate
//    // scope is set when signing in.
//    
//    
//    [self checkvalidate];
//}
//    -(void)checkvalidate
//    {
//        GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
//        if (person == nil) {
//            return;
//        }
//        
//        NSLog(@"name : %@",person.displayName);
//        NSLog(@"gender : %@",person.gender);
//        NSLog(@"Image : %@",[[GPPSignIn sharedInstance].googlePlusUser.image.url stringByReplacingOccurrencesOfString:@"sz=50" withString:@"sz=600"]);
//        NSLog(@"email : %@",[GPPSignIn sharedInstance].userEmail);
//        NSLog(@"id : %@",person.identifier);
//        
//        NSLog(@"dob : %@",[GPPSignIn sharedInstance].googlePlusUser.birthday);
//        
//       
//        [self loginWithGmail:person.displayName withEmail:[GPPSignIn sharedInstance].userEmail];
//        
//        
////        appdelegateobj.data_social=[[NSMutableDictionary alloc] init];
////        [appdelegateobj.data_social setObject:[arr objectAtIndex:0] forKey:@"first_name"];
////        [appdelegateobj.data_social setObject:[arr objectAtIndex:1] forKey:@"last_name"];
////        [appdelegateobj.data_social setObject:str_url forKey:@"photo"];
////        [appdelegateobj.data_social setObject:[GPPSignIn sharedInstance].userEmail  forKey:@"email"];
////        
////        [self checkvalidateEmail];
//        
//        
//    }

-(void)loginWithGmail:(NSString *)strName withEmail:(NSString *)email withImageURL : (NSString *)strImage{
    NSString *url=[NSString stringWithFormat:LoginWithGoogleURL];
    
    NSString * strUDID =  [[FIRInstanceID instanceID] token];
    if (strUDID == nil) {
        strUDID = @"1234568970";
    }
    
    NSDictionary *urlParam=@{@"Email":email,@"UserName":strName,@"DOB":@"17/07/1991",@"DeviceID":strUDID,@"Gender":@"Male",@"AboutMe":strImage};
    
    
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            NSMutableDictionary *dictMutable = [[receivedData valueForKey:@"data"] mutableCopy];
            if ([dictMutable valueForKey:@"ProfilePicDetails"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"ProfilePicDetails" ];
            }
            
            if ([dictMutable valueForKey:@"Gender"] == [NSNull null]) {
                [dictMutable setValue:@"Male" forKey:@"Gender" ];
            }
            if ([dictMutable valueForKey:@"Work"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Work" ];
            }
            if ([dictMutable valueForKey:@"Country"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Country" ];
            }
            if ([dictMutable valueForKey:@"City"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"City" ];
            }
            [[NSUserDefaults standardUserDefaults] setValue:dictMutable forKey:@"userInfo"];
            [self performSegueWithIdentifier:@"loginToHome" sender:self];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}


#pragma mark - WebService Login method
-(void)loginWebservice
{
    [newConnection cancelRequest];
    NSString *signInURLString=[NSString stringWithFormat:loginURL];
    
    NSString * strUDID =  [[FIRInstanceID instanceID] token];
    if (strUDID == nil) {
        strUDID = @"1234568970";
    }
    
    signInURLString = [signInURLString stringByAppendingString:[NSString stringWithFormat:@"Email=%@&Password=%@&DeviceID=%@",txtFieldEmail.text,txtFieldPassword.text,strUDID]];

  
    
    [self.view addSubview:indicator];
    
    [newConnection startConectionWithString:signInURLString HttpMethodType:Get_type HttpBodyType:nil Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            NSMutableDictionary *dictMutable = [[receivedData valueForKey:@"data"] mutableCopy];
            if ([dictMutable valueForKey:@"ProfilePicDetails"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"ProfilePicDetails" ];
            }
            if ([dictMutable valueForKey:@"City"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"City" ];
            }
            if ([dictMutable valueForKey:@"Country"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Country" ];
            }
            if ([dictMutable valueForKey:@"Work"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Work" ];
            }
            [[NSUserDefaults standardUserDefaults] setValue:dictMutable forKey:@"userInfo"];
            [[NSUserDefaults standardUserDefaults] setValue:@"Km" forKey:@"getUserDistence"];
            [self performSegueWithIdentifier:@"loginToHome" sender:self];
        }else{
            
            if ([[receivedData valueForKey:@"Message"] isEqualToString:@"User is Already Logged in Another Device. Please Log Out from that First"]) {
                
                userIDLogout = [[[receivedData valueForKey:@"data"] valueForKey:@"ID"] intValue];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"LogOut", nil];
                alert.tag = 101;
                [alert show];
            }else{
            
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
    }];
    
}

-(void)loginWithFb{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    }
    
    [login logInWithReadPermissions:@[@"public_profile", @"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops";
            [[[UIAlertView alloc] initWithTitle:alertTitle
                                        message:alertMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        else
        {
            if(result.token)   // This means if There is current access token.
            {
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"picture, name, email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id userinfo, NSError *error) {
                     if (!error) {
                         
                         NSDictionary *userData = [[NSDictionary alloc]initWithDictionary:userinfo];
                         [self loginWithFB:[userData objectForKey:@"id"] withName:[userData objectForKey:@"name"]];
                     }
                     else{
                         
                         NSLog(@"%@", [error localizedDescription]);
                     }
                 }];
            }
            NSLog(@"Login Cancel");
        }
    }];

}



- (IBAction)btTappedFacebook:(id)sender{
    [self loginWithFb];
}
    
    
-(void)loginWithFB:(NSString *)ID withName:(NSString *)strName{
    NSString *url=[NSString stringWithFormat:LoginWithFBURL];
    NSString * strUDID =  [[FIRInstanceID instanceID] token];
    if (strUDID == nil) {
        strUDID = @"1234568970";
    }
    
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", ID];
    NSDictionary *urlParam=@{@"Email":ID,@"UserName":strName,@"DOB":@"17/07/1991",@"DeviceID":strUDID,@"AboutMe":userImageURL};
    
    
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
       
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
             NSMutableDictionary *dictMutable = [[receivedData valueForKey:@"data"] mutableCopy];
            if ([dictMutable valueForKey:@"ProfilePicDetails"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"ProfilePicDetails" ];
            }
            if ([dictMutable valueForKey:@"City"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"City" ];
            }
            if ([dictMutable valueForKey:@"Country"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Country" ];
            }
            if ([dictMutable valueForKey:@"Work"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Work" ];
            }
            if ([dictMutable valueForKey:@"Gender"] == [NSNull null]) {
                [dictMutable setValue:@"" forKey:@"Gender" ];
            }
           // [[NSUserDefaults standardUserDefaults] setValue:dictMutable forKey:@"userInfo"];
            
            [[NSUserDefaults standardUserDefaults] setValue:dictMutable forKey:@"userInfo"];
            [[NSUserDefaults standardUserDefaults] setValue:@"Km" forKey:@"getUserDistence"];
            [self performSegueWithIdentifier:@"loginToHome" sender:self];

            dispatch_async(dispatch_get_main_queue(), ^{
                [indicator removeFromSuperview];

            });
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
     
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

@end
