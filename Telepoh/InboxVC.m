//
//  InboxVC.m
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "InboxVC.h"
#import "InboxCustomCell.h"
#import "ChatingVC.h"
#import "UserProfileVC.h"

@interface InboxVC ()
{
    NSMutableArray * arrUserList;
    Indicator * indicator;
    WebServiceConnection * newConnection;
    
    
    
}
@property (strong,nonatomic) NSString *strURLImg;

@end

@implementation InboxVC
@synthesize strLastController,strEventID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [[UITabBar appearance] setTintColor:[UIColor redColor]];
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
   
    arrUserList = [[NSMutableArray alloc] init];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if ([strLastController isEqualToString:@"PostedActivity"]) {
        [self postedEventUsersList];
    }else{
        [self InboxUsersList];
    }
   
}


-(void)viewWillAppear:(BOOL)animated{
    if ([strLastController isEqualToString:@"PostedActivity"]) {
        [self postedEventUsersList];
    }else{
        [self InboxUsersList];
    }
}
-(void)postedEventUsersList{
    NSString *url=[NSString stringWithFormat:JoinedActivityUsersURL];
    NSDictionary *urlParam=@{@"EventID":[NSNumber numberWithInt:[strEventID intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrUserList = [[receivedData valueForKey:@"data"] mutableCopy];
            [tblView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if (arrUserList.count == 0) {
            lblNoRecords.hidden = false;
            
        }else{
            lblNoRecords.hidden =true;
        }
        
    }];
}

-(void)InboxUsersList{
    NSString *url=[NSString stringWithFormat:AllUserMessagesURL];
    NSDictionary *urlParam=@{@"ID":[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrUserList = [[receivedData valueForKey:@"data"] mutableCopy];
            [tblView reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        if (arrUserList.count == 0) {
            lblNoRecords.hidden = false;
            
        }else{
            lblNoRecords.hidden =true;
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrUserList.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InboxCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    self.strURLImg = [arrUserList[indexPath.row] valueForKey:@"UserProfilePic"];
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
    cell.userImage.clipsToBounds = YES;
     [cell.userImage setImageURL:[NSURL URLWithString:self.strURLImg]];
    
    cell.lblUserName.text =[arrUserList[indexPath.row] valueForKey:@"UserName"];
    cell.lblUserDescription.text =[arrUserList[indexPath.row] valueForKey:@"LastMessage"];
    
    if ([[arrUserList[indexPath.row] valueForKey:@"Read"] boolValue] == false )
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    AsyncImageView *imgView = (AsyncImageView *)cell.userImage;
    
    NSString * strURLImg = [[arrUserList[indexPath.row] valueForKey:@"ProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    
    cell.btnUserProfile.tag = indexPath.row;
    
    [cell.btnUserProfile addTarget:self action:@selector(userProfileTapped:) forControlEvents:UIControlEventTouchUpInside];
   
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard;
    
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChatingVC"];
    vc.dictInfo = arrUserList[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];

}
-(void)userProfileTapped:(UIButton *)sender{
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileVC *dashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    NSMutableDictionary * dict = [arrUserList[sender.tag] mutableCopy];
    [dict removeObjectForKey:@"ID"];
    //    [dict removeObjectForKey:@"UserDetailsID"];
    [dict setValue:[dict objectForKey:@"UserID1"] forKey:@"UserDetailsID"];
    dashboardViewController.dictInfoUser = dict;
    [self.navigationController pushViewController:dashboardViewController animated:NO];
}

- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}
@end
