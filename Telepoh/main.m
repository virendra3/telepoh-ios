//
//  main.m
//  Telepoh
//
//  Created by Admin on 4/4/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
