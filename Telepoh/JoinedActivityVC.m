//
//  JoinedActivityVC.m
//  Telepoh
//
//  Created by apple on 23/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "JoinedActivityVC.h"
#import "InboxCustomCell.h"
@interface JoinedActivityVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    NSMutableArray * arrJoinedEventList;
}

@end

@implementation JoinedActivityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    
    arrJoinedEventList = [[NSMutableArray alloc] init];
    
    [self joinedActivity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)joinedActivity{
    
    NSString *url=[NSString stringWithFormat:JoinedUsersActivityURL];
    NSDictionary *urlParam=@{ID_Key:[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            arrJoinedEventList = [[receivedData valueForKey:@"data"] mutableCopy];
            [tblView reloadData];
        }else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
        if (arrJoinedEventList.count == 0) {
            lblNoRecords.hidden = false;
            
        }else{
            lblNoRecords.hidden =true;
        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrJoinedEventList.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InboxCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InboxCustomCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.height/2;
    cell.userImage.clipsToBounds = YES;
    
    AsyncImageView *imgView = (AsyncImageView *)cell.userImage;
    NSString * strURLImg = [[arrJoinedEventList[indexPath.row] valueForKey:@"UserProfilePic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [imgView setImageURL:[NSURL URLWithString:strURLImg]];
    
        cell.lblUserName.text =[arrJoinedEventList[indexPath.row] valueForKey:@"UserName"];
        cell.lblUserDescription.text =[arrJoinedEventList[indexPath.row] valueForKey:@"Title"];
//        cell.userImage.image = [UIImage imageNamed:arrImage[indexPath.row]];
    return cell;
    
}


//CurrentDistence = 2439;
//EventID = 541;
//MaxAllowedPeople = 0;
//TimeLimit = "2016-06-17T00:53:02.057";
//Title = dhjcc;
//UserDetailsID = 89;
//UserName = dakuu;
//UserProfilePic = "http://telepoh.shrigenesis.com/Content/Images/default.jpg";
//UserRatings = 0;



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}
- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}
@end
