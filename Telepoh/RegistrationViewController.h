//
//  RegistrationViewController.h
//  Telepoh
//
//  Created by apple on 04/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController
{
    IBOutlet UITextField *txtFieldEmail;
    IBOutlet UITextField *txttFieldCity;
    IBOutlet UITextField *txtFieldCountry;
    IBOutlet UITextField *txtFieldDOB;
    
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnMale;
    IBOutlet UITextField *txtFieldWork;
    IBOutlet UITextField *txtFieldUserName;
    IBOutlet UITextField *txtMobileNumber;
    IBOutlet UITextField *txtFieldPassword;
    IBOutlet UITextField *txtFieldRetypePassword;
    IBOutlet UIButton *btnRegistration;
}
- (IBAction)btnTappedRadio:(id)sender;

@end
