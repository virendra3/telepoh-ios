//
//  CollectionViewCell.h
//  Telepoh
//
//  Created by apple on 19/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "GraphView.h"

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnJoin;
@property (weak, nonatomic) IBOutlet UIButton *btnIgnore;
@property (weak, nonatomic) IBOutlet UIView *vwBackGround;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UIView *vwTimerLoader;
@property (weak, nonatomic) IBOutlet UILabel *lblMacimumJoiners;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentLocation;
@end
