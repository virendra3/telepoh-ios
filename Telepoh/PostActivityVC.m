//
//  PostActivityVC.m
//  Telepoh
//
//  Created by apple on 10/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "PostActivityVC.h"
#import "PostedActivityVC.h"



#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface PostActivityVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    Validation * validateObj;
    NSString * strLimited;
    NSString * strLat;
    NSString * strLong;
   
}

@end

@implementation PostActivityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    validateObj = [Validation validationManager];
    strLimited = @"0";
//    strLat = @"26.911060";
//    strLong = @"75.737356";
    
    vwPostActivity.layer.shadowRadius = 10.0;
    vwPostActivity.layer.cornerRadius = 10.0;
    vwPostActivity.clipsToBounds = YES;
    
    btnPostActivity.layer.cornerRadius = 10.0;
    btnPostActivity.clipsToBounds = YES;
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
   
        locationManager = [[CLLocationManager alloc] init];
        
        locationManager.delegate = self;
        if(IS_OS_8_OR_LATER){
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                    [locationManager requestAlwaysAuthorization];
                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [locationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        [locationManager startUpdatingLocation];
    
    

}
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        strLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        strLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnTappedPostActivity:(id)sender {
    NSString *message = nil;
    
    if (![validateObj validateBlankField:[txtFieldTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        message =  @"Please enter Title";
    }
    else if ([txtfieldMins.text intValue] < 10 && [txtFieldHrs.text intValue]  <= 0)
    {
        // message =  @"Please enter Hours";
        message = @"Time should be atleast 10 minutes";
    }
  /*  else if (![validateObj validateBlankField:[txtFieldHrs.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] && [txtfieldMins.text intValue] < 10 && [txtFieldHrs.text intValue]  >= 0)
    {
       // message =  @"Please enter Hours";
        message = @"Time should be atleast 10 minutes";
    }
    
    else if (![validateObj validateBlankField:[txtfieldMins.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] )
    {
        message =  @"Please enter Minutes";
        
    }else if([txtFieldHrs.text intValue] == 0){
        if([txtfieldMins.text intValue] < 10){
        
        message =  @"Minutes should be greater than 10 minutes";
        }
    }
     */
    
    
    if ([strLimited isEqualToString:@"1"]) {
        if (![validateObj validateBlankField:[txtFieldLimited.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
        {
            message =  @"Activity joiners count missing";
        }

    }
    
    if ([message length]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else{
        [self addEventWebService];
    }
}

#pragma mark - WebService addEventWebService method
-(void)addEventWebService
{

    //SS
    if (![validateObj validateBlankField:[txtFieldHrs.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
        txtFieldHrs.text = @"0";
    if (![validateObj validateBlankField:[txtfieldMins.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
        txtfieldMins.text = @"0";
    
    
    NSString * strURL =[NSString stringWithFormat:addEventURL];
    
    NSString * strPeopleAllowed ;
    if ([strLimited isEqualToString:@"0"]) {
        strPeopleAllowed = @"No Limit";
        txtFieldLimited.text = @"";
        
    }else{
        strPeopleAllowed = @"Limited";
    }
    NSString * strCountMember ;
    if (txtFieldLimited.text.length < 1) {
        strCountMember = @"0";
    }else{
        strCountMember = txtFieldLimited.text;
    }
    
    //strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSDictionary *user = [[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"];
    NSDictionary *urlParam=@{@"Title":txtFieldTitle.text,@"Hour":txtFieldHrs.text,@"Minutes":txtfieldMins.text,@"PeopleAllowed":strPeopleAllowed,@"PeopleNumber":strCountMember,@"Longitude":strLong,@"Lattitude": strLat , @"UserID":[user objectForKey:@"ID"]};
    
    [self.view addSubview:indicator];
    
 //   [newConnection startConectionWithString:strURL HttpMethodType:Get_type HttpBodyType:nil Output:^(NSDictionary *receivedData) {
    [newConnection startConectionWithString:strURL HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        //NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
       
        
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
           
            txtFieldHrs.text = @"";
            txtFieldLimited.text = @"";
            txtfieldMins.text = @"";
            txtFieldTitle.text = @"";
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PostedActivityVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"PostedActivityVC"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else{
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }];
    
}
- (IBAction)btnTappedMenu:(id)sender {
     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController revealToggle:sender];
}

- (IBAction)btnTappedRadioButton:(id)sender {
    UIButton * btn  = sender;
    
    if (btn.tag == 1) {
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        [btnLimited setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        txtFieldLimited.userInteractionEnabled = NO;
        txtFieldLimited.text = @""; // if u wanna show 
        strLimited = @"0";
    }else{
        [btn setImage:[UIImage imageNamed:@"act-select"] forState:UIControlStateNormal];
        [btnNoLimited setImage:[UIImage imageNamed:@"dis-select"] forState:UIControlStateNormal];
        txtFieldLimited.userInteractionEnabled = YES;
        strLimited = @"1";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
#define ACCEPTABLE_CHARACTERS @"0123456789"
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == txtFieldHrs  || textField == txtFieldLimited) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        NSString * strFull = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        
        if ([string isEqualToString:filtered] == false) {
            return false;
        }else{
            if (textField == txtFieldHrs) {
                
                if ([strFull integerValue] <= 23) {
                    return string;
                }else{
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Activity Time Should be less than or equals to 24 hours." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    return false;
                }
            }
            
        }
        
        //        return [string isEqualToString:filtered];
    }
    
    
    if ( textField == txtfieldMins) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        NSString * strFull = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        
        if ([string isEqualToString:filtered] == false) {
            return false;
        }else{
            if ([strFull integerValue] < 60) {
                return string;
            }else{
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Activity Time Should be less than or equals to 59 minute." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                return false;
            }
            
            
        }
    }
    if (textField == txtFieldTitle) {
        if (txtFieldTitle.text.length < 100) {
            return string;
        }else{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Only 100 Characters are allowed." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
    return string;
}
@end














