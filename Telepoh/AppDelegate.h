//
//  AppDelegate.h
//  Telepoh
//
//  Created by Admin on 4/4/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
@import FirebaseMessaging;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

