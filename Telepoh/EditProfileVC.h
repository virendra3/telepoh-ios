//
//  EditProfileVC.h
//  Telepoh
//
//  Created by apple on 15/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextViewDelegate>
{
    IBOutlet UIImageView *imgUser1;
    IBOutlet UIImageView *imgUser2;
    IBOutlet UIImageView *imgUser3;
    IBOutlet UIImageView *imgUser4;
    IBOutlet UIImageView *imgUser5;
    
    IBOutlet UIButton *btnBothInterest;
    IBOutlet UIButton *btnFemaleInterest;
    IBOutlet UIButton *btnMaleInterest;
    IBOutlet UIButton *btnKilometer;
    IBOutlet UIButton *btnMiles;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnMale;
    
    
    
    IBOutlet UISlider *slider;
    IBOutlet UILabel *lblSearchMeter;
    
    IBOutlet UITextView *txtFieldAboutMe;
    IBOutlet UITextField *txtFieldTitle;
    IBOutlet UITextField *txtFieldWork;
    IBOutlet UITextField *txtFieldCountry;
    IBOutlet UITextField *txtFieldCity;
    
    IBOutlet UITextField *txtField_DOB;

}
- (IBAction)btnTappedGender:(id)sender;
- (IBAction)btnTappedDistanceUnit:(id)sender;
- (IBAction)btnTappedInterest:(id)sender;
- (IBAction)sliderValueChange:(id)sender;
- (IBAction)btnTappedSave:(id)sender;

@end
