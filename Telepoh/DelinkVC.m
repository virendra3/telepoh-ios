//
//  DelinkVC.m
//  Telepoh
//
//  Created by apple on 21/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "DelinkVC.h"
#import "HomeVC.h"
#import "InboxVC.h"

@interface DelinkVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;

}

@end

@implementation DelinkVC
@synthesize dictInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    vcBackGround.layer.cornerRadius = 10.0;
    vcBackGround.clipsToBounds = YES;
    
    //lblTitle.text = [NSString stringWithFormat:@"Are you sure ? Do you really want to Delink %@",[dictInfo valueForKey:@"UserName"]];
    lblTitle.text = [NSString stringWithFormat:@"Delink User %@",[dictInfo valueForKey:@"UserName"]];
    
    
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnTappedYes:(id)sender {
    
    UIButton * btn =sender;
    
    if (btn.tag == 1) {
        [dictInfo valueForKey:@"ID"];
        [self sendMessageWebservice];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void)sendMessageWebservice{
    NSString *url=[NSString stringWithFormat:DelinkUserURL];
    NSDictionary *urlParam=@{@"ID":[dictInfo valueForKey:@"ID"]};
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        
        
        
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"removeDelinkView"
             object:self];
//            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            InboxVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"InboxVC"];
//           [self.navigationController pushViewController:vc animated:YES];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[receivedData valueForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK-TP2" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
}
@end
