//
//  ChangePassword.h
//  Telepoh
//
//  Created by apple on 10/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassword : UIViewController
{
    IBOutlet UIView *vwChangePassword;
    
    IBOutlet UIButton *btnChangePassword;
    IBOutlet UITextField *txtFieldConfirmPasswrd;
    IBOutlet UITextField *txtField_NewPassword;
    IBOutlet UITextField *txtfield_OldPassword;
}
- (IBAction)btnTappedMenu:(id)sender;
- (IBAction)btnTappedChangePassword:(id)sender;

@end
