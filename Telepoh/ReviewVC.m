//
//  ReviewVC.m
//  Telepoh
//
//  Created by apple on 24/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import "ReviewVC.h"

@interface ReviewVC ()
{
    Indicator * indicator;
    WebServiceConnection * newConnection;
    double ratingPoints;
}

@end

@implementation ReviewVC
@synthesize dictInfoEvent;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indicator= [[Indicator alloc] initWithFrame:self.view.frame];
    newConnection=[WebServiceConnection connectinManager];
    ratingPoints = 0.0;
    
    NSLog(@"%@",dictInfoEvent);
    
    lblName.text = [dictInfoEvent valueForKey:@"OtherUserName"];
    
    starReview.starImage =[UIImage imageNamed:@"star"];
    starReview.starHighlightedImage = [UIImage imageNamed:@"starActive"];
    starReview.maxRating = 5.0;
    starReview.delegate = self;
    starReview.editable=YES;
    starReview.rating= 0.0;
    starReview.displayMode=EDStarRatingDisplayHalf;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnTappedSubmit:(id)sender {
    if (ratingPoints != 0.0 ) {
     
        [self RatingsAdd];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hey User!" message:@"Please Give a Rating & Review." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)RatingsAdd{
    
    NSString *url=[NSString stringWithFormat:RatingsAddURL];
    
    
    NSDictionary *urlParam=@{@"GivenBy":[NSNumber numberWithInt:[[[[NSUserDefaults standardUserDefaults] valueForKey:@"userInfo"] valueForKey:@"ID"] intValue]] ,@"UserID":[NSNumber numberWithInt:[[dictInfoEvent valueForKey:@"OtherUserID"]intValue]],@"Ratings":[NSNumber numberWithDouble:ratingPoints],@"Review":txtReviewDescription.text,@"ID":[dictInfoEvent valueForKey:@"ID"]};
    
    
    [self.view addSubview:indicator];
    [newConnection startConectionWithString:url HttpMethodType:Post_Type HttpBodyType:urlParam Output:^(NSDictionary *receivedData) {
        NSLog(@"%@",receivedData);
        [indicator removeFromSuperview];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"SubmitReview"
         object:self];
        if ([[receivedData valueForKey:@"ReplyCode"]intValue] == 1) {
           
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks!" message:@"For your Rating." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
           
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Cancelled!" message:@"You have already rated this user in previous activity." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self dismissViewControllerAnimated:YES completion:nil];

    }];
}

-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
    ratingPoints = rating;
   
}
- (IBAction)btnTappedCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Write a comment"]) {
        textView.text =@"";
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write a comment";
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    if ([textView.text isEqualToString:@""]) {
//        textView.text = @"Your Reviews";
//    }
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}


@end
