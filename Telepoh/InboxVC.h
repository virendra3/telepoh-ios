//
//  InboxVC.h
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UILabel *lblNoRecords;
    IBOutlet UITableView *tblView;
}
@property (nonatomic, retain)NSString * strLastController;
@property (nonatomic, retain)NSString * strEventID;
- (IBAction)btnTappedMenu:(id)sender;

@end
