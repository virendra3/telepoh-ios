//
//  PostActivityVC.h
//  Telepoh
//
//  Created by apple on 10/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PostActivityVC : UIViewController<CLLocationManagerDelegate,UITextFieldDelegate>
{
    IBOutlet UIView         *  vwPostActivity;
    
    IBOutlet UITextField *txtFieldLimited;
    IBOutlet UIButton       *  btnPostActivity;
    IBOutlet UITextField    *  txtFieldTitle;
    IBOutlet UITextField    *  txtFieldHrs;
    IBOutlet UITextField    *  txtfieldMins;
    CLLocationManager * locationManager;
    
    IBOutlet UIButton *btnNoLimited;
    IBOutlet UIButton *btnLimited;
}
- (IBAction)btnTappedMenu:(id)sender;
- (IBAction)btnTappedRadioButton:(id)sender;

@end
