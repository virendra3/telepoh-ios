//
//  UserProfileVC.h
//  Telepoh
//
//  Created by apple on 09/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface UserProfileVC : UIViewController
{
    IBOutlet UIImageView *profileImg;
    IBOutlet UILabel *profileName;
    IBOutlet UILabel *profilePost;
    
    IBOutlet UITableView *tblView;
    IBOutlet EDStarRating *vwAvgRating;
    
    IBOutlet UILabel *lblNoRecords;
    IBOutlet UILabel *lblAvgRating;
    IBOutlet UILabel *lblRating1;
    IBOutlet UILabel *lblRating2;
    IBOutlet UILabel *lblRating3;
    IBOutlet UILabel *lblRating4;
    IBOutlet UILabel *lblRating5;
    IBOutlet UILabel *totalReview;
}
@property(nonatomic ,retain)NSDictionary * dictInfoUser;
- (IBAction)btnTappedMenu:(id)sender;

@end
