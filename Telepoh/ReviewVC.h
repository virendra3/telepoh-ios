//
//  ReviewVC.h
//  Telepoh
//
//  Created by apple on 24/04/16.
//  Copyright (c) 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface ReviewVC : UIViewController<EDStarRatingProtocol,UITextViewDelegate>{
    
    IBOutlet UILabel *lblName;
    IBOutlet EDStarRating *starReview;
    IBOutlet UITextView *txtReviewDescription;
}
@property(nonatomic,retain)NSDictionary * dictInfoEvent;

@end
