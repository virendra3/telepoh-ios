//
//  ViewController.h
//  Telepoh
//
//  Created by Admin on 4/4/16.
//  Copyright © 2016 Abc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,GIDSignInDelegate, GIDSignInUIDelegate>
{
    IBOutlet UITextField *txtFieldEmail;
    IBOutlet UITextField *txtFieldPassword;
    IBOutlet UIButton *btnLogin;
   
    
}
- (IBAction)btnTappedLogin:(id)sender;
- (IBAction)btnTappedGoogleLogin:(id)sender;

- (IBAction)btnTappedForget:(id)sender;
@end

